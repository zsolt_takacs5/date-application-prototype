﻿namespace DateAPI.Models
{
    using System.Collections.Generic;
    using AppMutualInterfaces;

    public abstract class Profile : IProfile
    {
        private int userID;
        private string userName;
        private int age;
        private bool male;
        protected object plusData;
        protected ProfileType profileType;
        protected bool verified;
        protected Dictionary<string, bool> abilities;

        public int UserID { get => userID;}
        public string UserName { get => userName;}
        public int Age { get => age;}
        public bool Male { get => male;}
        public object PlusData { get => plusData; }
        public string ProfileType { get => profileType.ToString(); }
        public bool Verified { get => verified; }
        public Dictionary<string, bool> Abilities { get { return this.abilities; } set { this.abilities = value; } }


        protected Profile(int userID, string userName, int age, bool male, object data)
        {
            this.userID = userID;
            this.userName = userName;
            this.age = age;
            this.male = male;
            this.plusData = data;
            this.abilities = new Dictionary<string, bool>();
        }
    }
}