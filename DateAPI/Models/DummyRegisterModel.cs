﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;

    public class DummyRegisterModel : IRegisterable
    {
        private bool gender;
        private bool interested;
        private int minAge;
        private int maxAge;
        private int age;
        private string name;
        private string email;
        private string password;

        public bool Gender { get => gender; set => gender = value; }
        public bool Interested { get => interested; set => interested = value; }
        public int MinAge { get => minAge; set => minAge = value; }
        public int MaxAge { get => maxAge; set => maxAge = value; }
        public int Age { get => age; set => age = value; }
        public string Name { get => name; set => name = value; }
        public string EMail { get => email; set => email = value; }
        public string PassW { get => password; set => password = value; }

        public DummyRegisterModel(bool gender, bool interested, int minAge, int maxAge, int age, string name, string email, string password)
        {
            this.gender = gender;
            this.interested = interested;
            this.minAge = minAge;
            this.maxAge = maxAge;
            this.age = age;
            this.name = name;
            this.email = email;
            this.password = password;
        }
     
        
    }
}