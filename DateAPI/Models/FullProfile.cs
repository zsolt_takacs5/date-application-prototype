﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;

    public class FullProfile : Profile
    {
        private string photo1;
        private string photo2;
        private string photo3;
        private string photo4;
        private string photo5;

        public string Photo1 { get => photo1; }
        public string Photo2 { get => photo2; }
        public string Photo3 { get => photo3; }
        public string Photo4 { get => photo4; }
        public string Photo5 { get => photo5; }

        public FullProfile(IUser user) :base(user.UserID,user.Nickname,user.Age, user.Male, null)
        {
            photo1 = user.Photo1;
            photo2 = user.Photo2;
            photo3 = user.Photo3;
            photo4 = user.Photo4;
            photo5 = user.Photo5;
            base.profileType = AppMutualInterfaces.ProfileType.FullProfile;
            base.verified = user.Verified;
        }
        public FullProfile(IUser user, object plusData) : this(user)
        {
            base.plusData = plusData;
            base.profileType = AppMutualInterfaces.ProfileType.FullProfile;
            base.verified = user.Verified;
        }
    }
}