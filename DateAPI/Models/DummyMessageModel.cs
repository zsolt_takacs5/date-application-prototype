﻿namespace DateAPI.Models
{
    using System;
    using System.Collections.Generic;
    using AppMutualInterfaces;

    public class DummyMessageModel : IMessage
    {
        private int messageID;
        private int? matchID;
        private int senderID;
        private DateTime messageDate;
        private string content;
        private string contentPhoto;
        private string contentType;
        private int? dateID;
        private bool read;

        public int MessageID { get { return this.messageID; } }
        public int? MatchID { get { return this.matchID; } }
        public int SenderID { get { return this.senderID; }}
        public DateTime MessageDate { get { return this.messageDate; }}
        public string Content { get { return this.content; } }
        public string ContentPhoto { get { return this.contentPhoto; } }
        public string ContentType { get { return this.contentType; } }
        public int? DateID { get { return this.dateID; } }
        public bool Read { get { return this.read; } set { this.read = value; } }


        public DummyMessageModel(IMessage message)
        {
            this.content = message.Content;
            this.contentPhoto = message.ContentPhoto;
            this.contentType = message.ContentType;
            this.matchID = message.MatchID;
            this.messageDate = message.MessageDate;
            this.messageID = message.MessageID;
            this.senderID = message.SenderID;
            this.dateID = message.DateID;
            this.read = message.Read;
        }
        public static ICollection<DummyMessageModel> Convert(ICollection<IMessage> messages)
        {
            ICollection<DummyMessageModel> newMessages = new List<DummyMessageModel>();
            foreach (var message in messages)
            {
                newMessages.Add(new DummyMessageModel(message));
            }
            return newMessages;
        } 
    }
}