﻿namespace DateAPI.Models
{
    using System.Collections.Generic;
    using AppMutualInterfaces;

    public class ProfileMessageWrapper
    {
        private IProfile profile;
        private ICollection<IMessage> messages;

        public ProfileMessageWrapper(IProfile profile, ICollection<IMessage> messages)
        {
            this.profile = profile;
            this.messages = messages;
        }

        public IProfile Profile { get => profile; }
        public ICollection<IMessage> Messages { get => messages; }
        
    }
}