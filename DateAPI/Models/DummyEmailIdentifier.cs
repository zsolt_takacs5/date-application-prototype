﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;
    public class DummyEmailIdentifier : IEmailIdentifiable
    {
        private string eMail;
        public string EMail { get { return this.eMail; } set { eMail = value; } }
        public DummyEmailIdentifier(string mail)
        {
            this.eMail = mail;
        }
    }
}