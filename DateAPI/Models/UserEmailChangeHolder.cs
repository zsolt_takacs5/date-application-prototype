﻿namespace DateAPI.Models
{
    public class UserEmailChangeHolder : UserCredentials
    {
        public string NewEmail { get; set; }
    }
}