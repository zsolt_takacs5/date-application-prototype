﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;

    public class RestrictedProfile : Profile
    {
        
        public RestrictedProfile(IUser user) : base(user.UserID,user.Nickname,user.Age, user.Male, null)
        {
            base.profileType = AppMutualInterfaces.ProfileType.RestrictedProfile;
            base.verified = user.Verified;
        }
    }
}