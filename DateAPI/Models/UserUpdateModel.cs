﻿namespace DateAPI.Models
{
    public class UserUpdateModel
    {
        public string UserName { get; set; }
        public int Age { get; set; }
        public bool Male { get; set; }
        public bool Interested { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
    }
}