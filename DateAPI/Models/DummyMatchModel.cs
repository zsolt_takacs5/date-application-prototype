﻿namespace DateAPI.Models
{
    using System;
    using AppMutualInterfaces;

    public class DummyMatchModel : IMatch
    {
        private int matchID;
        private int user1_ID;
        private int user2_ID;
        private DateTime matchDate;

        public int MatchID { get { return this.matchID; } }
        public int User1_ID { get { return this.user1_ID; } }
        public int User2_ID { get { return this.user2_ID; } }
        public DateTime MatchDate { get { return this.matchDate; } }

        public DummyMatchModel(IMatch match)
        {
            this.matchDate = match.MatchDate;
            this.matchID = match.MatchID;
            this.user1_ID = match.User1_ID;
            this.user2_ID = match.User2_ID;
        }

    }
}