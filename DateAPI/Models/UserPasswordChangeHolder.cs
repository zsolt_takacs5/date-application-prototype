﻿namespace DateAPI.Models
{
    public class UserPasswordChangeHolder : UserCredentials
    {
        public string NewPassword { get; set; }
    }
}