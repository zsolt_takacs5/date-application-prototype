﻿namespace DateAPI.Models
{
    using System;
    using AppMutualInterfaces;

    public class FullUserProfile : Profile
    {
        string email;
        bool interested;
        int minAge;
        int maxAge;
        string photo1;
        string photo2;
        string photo3;
        string photo4;
        string photo5;
        DateTime registeredDate;
        //bool verified;
        int? subscriptionLevel;
        public FullUserProfile(IUser user) : base(user.UserID,user.Nickname,user.Age,user.Male, null)
        {
            this.email = user.EMail;
            this.interested = user.Interested;
            this.minAge = user.MinAge;
            this.maxAge = user.MaxAge;
            this.photo1 = user.Photo1;
            this.photo2 = user.Photo2;
            this.photo3 = user.Photo3;
            this.photo4 = user.Photo4;
            this.photo5 = user.Photo5;
            this.registeredDate = user.RegisteredDate;
            base.verified = user.Verified;
            this.subscriptionLevel = user.SubscriptionLevel;
            base.profileType = AppMutualInterfaces.ProfileType.FullUserProfile;
        }
        public FullUserProfile(IUser user, object plusData) : this(user)
        {
            base.plusData = plusData;
        }

        public string Email { get => email;}
        public bool Interested { get => interested;}
        public int MinAge { get => minAge;}
        public int MaxAge { get => maxAge;}
        public string Photo1 { get => photo1;}
        public string Photo2 { get => photo2;}
        public string Photo3 { get => photo3;}
        public string Photo4 { get => photo4;}
        public string Photo5 { get => photo5;}
        public DateTime RegisteredDate { get => registeredDate;}
        //public bool Verified { get => verified;}
        public int? SubscriptionLevel { get => subscriptionLevel;}
    }
}