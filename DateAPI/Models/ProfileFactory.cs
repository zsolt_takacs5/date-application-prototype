﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;

    public class ProfileFactory : IProfileFactory
    {
        public IProfile GetProfile(IUser user, ProfileType profileType)
        {
            switch (profileType)
            {
                case ProfileType.RestrictedProfile:
                    return new RestrictedProfile(user);
                    break;
                case ProfileType.FullProfile:
                    return new FullProfile(user);
                    break;
                case ProfileType.FullUserProfile:
                    return new FullUserProfile(user);
                    break;
                default:
                    return new RestrictedProfile(user);
            }
        }
    }
}