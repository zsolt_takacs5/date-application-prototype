﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;
    using System.Collections.Generic;

    public class MatchDateModel
    {
        private List<ProfileMessageWrapper> matches;
        private ProfileMessageWrapper date;

        public List<ProfileMessageWrapper> Matches
        {
            get { return this.matches; }
        }
        public ProfileMessageWrapper Date
        {
            get { return this.date; }
        }

        public MatchDateModel(Dictionary<IProfile, IMessage> inputMatches, Dictionary<IProfile, IMessage> inputDate)
        {
            matches = new List<ProfileMessageWrapper>();
            date = null;
            foreach (var match in inputMatches)
            {
                List<IMessage> m = new List<IMessage>();
                m.Add(match.Value);
                matches.Add(new ProfileMessageWrapper(match.Key, m));
            }
            foreach (var d in inputDate)
            {
                List<IMessage> m = new List<IMessage>();
                m.Add(d.Value);
                date = new ProfileMessageWrapper(d.Key, m);
            }
        }
    }
}