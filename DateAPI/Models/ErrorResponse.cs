﻿namespace DateAPI.Models
{
    public class ErrorResponse
    {
        private string message;

        public string Message { get { return message; } }

        public ErrorResponse(string message)
        {
            this.message = message;
        }
    }
}