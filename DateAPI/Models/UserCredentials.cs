﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;

    public class UserCredentials : ICredentialable
    {
        public string EMail { get; set; }
        public string PassW { get; set; }
    }
}