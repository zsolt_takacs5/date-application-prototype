﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;
    public class DummyUserLoginRegister : ICredentialable
    {
        private string _email;
        private string _password;

        public string EMail { get { return this._email; } set { this._email = value; } }
        public string PassW { get { return this._password; } set { this._password = value; } }

        public DummyUserLoginRegister(string mail, string passw)
        {
            this._email = mail;
            this._password = passw;
        }
    }

}