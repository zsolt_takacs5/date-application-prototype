﻿namespace DateAPI.Models
{
    using AppMutualInterfaces;
    using System;

    
    
    public class DummyUserModel : IUser
    {
        private int _UserId;
        private string _NickName;
        private string _PassW;
        private int _Age;
        private bool _Male;
        private string _Gender;
        private bool _Interested;
        private string _InterestedString;
        private int _MinAge;
        private int _MaxAge;
        private string _Photo1;
        private string _Photo2;
        private string _Photo3;
        private string _Photo4;
        private string _Photo5;
        private string _Live1;
        private string _Live2;
        private string _Live3;
        private bool _Verified;
        private bool _WantToDate;
        private int? _SubscriptionLevel;
        private DateTime? _SubscriptionDate;
        private DateTime _RegisteredDate;
        private string _EMail;
        private int? _CurrentDate;


        public DummyUserModel(IUser user)
        {
            this._EMail = user.EMail;
            this._Age = user.Age;
            this._Male = user.Male;
            this._Gender = GenderToString(this.Male);
            this._Interested = user.Interested;
            this._Live1 = user.Live1;
            this._Live2 = user.Live2;
            this._Live3 = user.Live3;
            this._MaxAge = user.MaxAge;
            this._MinAge = user.MinAge;
            this._NickName = user.Nickname;
            this._Photo1 = user.Photo1;
            this._Photo2 = user.Photo2;
            this._Photo3 = user.Photo3;
            this._Photo4 = user.Photo4;
            this._Photo5 = user.Photo5;
            this._RegisteredDate = user.RegisteredDate;
            this._SubscriptionDate = user.SubscriptionDate;
            this._SubscriptionLevel = user.SubscriptionLevel;
            this._UserId = user.UserID;
            this._Verified = user.Verified;
            this._WantToDate = user.WantToDate;
            this._CurrentDate = user.CurrentDate;
        }
        private DummyUserModel(string email)
        {
            this._EMail = email;
        }
        public static IUser CreateUser(string eMail)
        {
            return new DummyUserModel(eMail);
        }
        public string EMail { get => _EMail; set => _EMail = value; }
        public string PassW { get => String.Empty; set => _PassW = string.Empty; }
        public int UserID { get => _UserId; set => _UserId = value; }
        public string Nickname { get => _NickName; set => _NickName = value; }
        public int Age { get => _Age; set => _Age = value; }
        public bool Male { get => _Male; set => _Male = value; }
        public string Gender { get => this.GenderToString(_Male); set => _Gender = value; }
        public bool Interested { get => this._Interested; set => this._Interested = value; }
        public string InterestedString { get => this.GenderToString(_Interested); set => _InterestedString = value; }
        public int MinAge { get => _MinAge; set => _MinAge = value; }
        public int MaxAge { get => _MaxAge; set => _MaxAge = value; }
        public string Photo1 { get => _Photo1; set => _Photo1 = value; }
        public string Photo2 { get => _Photo2; set => _Photo2 = value; }
        public string Photo3 { get => _Photo3; set => _Photo3 = value; }
        public string Photo4 { get => _Photo4; set => _Photo4 = value; }
        public string Photo5 { get => _Photo5; set => _Photo5 = value; }
        public string Live1 { get => _Live1; set => _Live1 = value; }
        public string Live2 { get => _Live2; set => _Live2 = value; }
        public string Live3 { get => _Live3; set => _Live3 = value; }
        public bool Verified { get => _Verified; set => _Verified = value; }
        public bool WantToDate { get => _WantToDate; set => _WantToDate = value; }
        public int? SubscriptionLevel { get => _SubscriptionLevel; set => _SubscriptionLevel = value; }
        public DateTime? SubscriptionDate { get => _SubscriptionDate; set => _SubscriptionDate = value; }
        public DateTime RegisteredDate { get => _RegisteredDate; set => _RegisteredDate = value; }

        public int? CurrentDate { get => _CurrentDate; set => _CurrentDate = value; }

        private string GenderToString(bool condition)
        {
            return condition ? "Male" : "Female";
        }
    }
}