﻿namespace DateAPI
{
    using System.Linq;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using AuthenticationLibrary;

    public class JWTCustomAuthorizationAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {

            Authenticator authenticator = new Authenticator();
            
            //SOURCE: https://stackoverflow.com/questions/41568050/how-do-i-override-onauthorization-in-net-core-web-api
            if (actionContext.Request.Headers.Authorization != null)
            {
                ClaimsPrincipal claims;
                var authorization = actionContext.Request.Headers.Authorization;

                bool validToken = authenticator.ValidateToken(authorization.Parameter.ToString(), out claims);
                if (!validToken || authorization.Scheme != "Bearer")
                {
                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }
                else
                {
                    //base.Users = claims.Claims.FirstOrDefault().Value;
                    //Thread.CurrentPrincipal = claims;
                    actionContext.Request.Properties.Add("UserID", claims.Claims.Where(x => x.Type == "UserID").FirstOrDefault().Value);
                }
            }
            else
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
        }
    }
}