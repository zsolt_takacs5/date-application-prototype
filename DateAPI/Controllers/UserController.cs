﻿namespace DateAPI.Controllers
{
    using AppMutualInterfaces;
    using AuthenticationLibrary;
    using BusinessLogic;
    using DateAPI.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web;
    using System.Web.Http.Cors;
    using Repository;
    using Repository.Exceptions;
    using BusinessLogic.Exceptions;
    

    //Enabling CORS
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        /*
         * TESTUSER: 
         *      "EMail" : "testZsolt@gmail.com"
	            "PassW" : "testZsolti12345"

            Authorization header for requests: Bearer <token>
            token is:
            eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOiIxMDAyIiwiRW1haWwiOiJ0ZXN0WnNvbHRAZ21haWwuY29tIiwibmJmIjoxNTg0MDYzMjg4LCJleHAiOjE1ODQwNjUwODIsImlhdCI6MTU4NDA2MzI4OCwiaXNzIjoiRGF0ZUFwcGxpY2F0aW9uIn0.o0dQWprxmeM6ZsRY_sqiWEVVCkyq1N85SUlUVme-IY4
         */
        private ILogic logic;
        private int messageLimit;
        public UserController()
        {
            messageLimit = 3;
            logic = new Logic(this.messageLimit);
        }
        
        /// <summary>
        /// Register the user to the DB.
        /// </summary>
        /// <param name="user">The user to be registered.</param>
        /// <returns>HTTP 200-OK if the registration succeded, otherwise HTTP 409-Conflict.</returns>
        [AllowAnonymous]
        [Route("api/user/register")]
        [HttpPost]
        public HttpResponseMessage Register()
        {
            

            if (!Request.Content.IsMimeMultipartContent())
            {
                return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType, "Unsupported media type. Use multipart/form-data.");
            }
            var userReq = HttpContext.Current.Request.Params["user"];
            var photoReq = HttpContext.Current.Request.Params["photo"];
            var user = JsonConvert.DeserializeObject<DummyRegisterModel>(userReq);
            var photo = HttpContext.Current.Request.Files.Get("photo");

            if (user == null || string.IsNullOrEmpty(user.EMail) || string.IsNullOrEmpty(user.PassW) || photo == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid parameter(s)!");
            }
            if (user.MinAge > user.MaxAge)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Minimum age must be less or equal to maximum age!");
            }
            try
            {

                try
                {
                    var uploadedPath = ImageProcesser.UploadPhoto(photo, user.EMail);
                    logic.RegisterUser(user, uploadedPath);
                }
                catch (FormatException ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "The provided photo was invalid. " + ex.Message);
                }
            }
            catch (MultipleEmailException ex)
            {
                return Request.CreateResponse(HttpStatusCode.Conflict, ex.Message);
            }
            catch (ArgumentException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Egy kep feltoltese kotelezo!");
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("api/user/login")]
        public IHttpActionResult Login(UserCredentials user)
        {
            IUser userFromDB;

            if (user == null || string.IsNullOrEmpty(user.EMail) || string.IsNullOrEmpty(user.PassW))
                return BadRequest("HTTP Request body can not be NULL or Empty.");

            try
            {
                userFromDB = logic.FindUserByLoginCredentials(user);
            }
            catch (WrongCredentialsException ex)
            {
                return BadRequest(ex.Message);
            }

            Authenticator authenticator = new Authenticator();
            var token = authenticator.CreateJWT(userFromDB);
            return Ok(token);
        }

        [JWTCustomAuthorization]
        [HttpPost]
        [Route("api/user/deletephoto/{location:int}")]
        public IHttpActionResult DeletePhoto(int location)
        {
            int locMinBoundary = 1;
            int locMaxBoundary = 5;

            IUser user = GetUserFromContext();

            if (location < locMinBoundary || location > locMaxBoundary)
            {
                return BadRequest($"Invalid location. Please provide a location between {locMinBoundary}-{locMaxBoundary}");
            }
            string filePath = logic.DeletePhoto(user, location);
            if (!String.IsNullOrEmpty(filePath))
            {
                try
                {
                    var serverPath = HttpContext.Current.Server.MapPath(filePath);
                    ImageProcesser.DeletePhoto(serverPath);
                }
                catch (FileNotFoundException ex)
                {
                    return InternalServerError(ex);
                }
            }
            return Ok();

        }
        
        [JWTCustomAuthorization]
        [HttpPost]
        [Route("api/user/sharealbum/{userID:int?}")]
        public IHttpActionResult ShareAlbum(int userID)
        {
            var user = GetUserFromContext();
            IUser receiver;
            try
            {
                receiver = logic.FindUserByID(userID);
            }
            catch (NotFoundException<IUser> ex)
            {
                return BadRequest(ex.Message);
            }
            try
            {
                logic.AlbumShareSecured(user, receiver);
            }
            catch (NotSupportedActivityException ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        //TODO DONE - Should return an abstract Profile, based on the relation between the 2 users. Possible returns could be: RestrictedProfile, FullProfile, UserProfile
        //userID is an optional parameter. api call works without providing an ID -> returns the user data from the token
        [JWTCustomAuthorization]
        [HttpGet]
        [Route("api/user/viewprofile/{userID:int?}")]
        public IProfile ViewProfile(int userID = -1)
        {
            IUser user = GetUserFromContext();
            IUser requestedUser = null;
            try
            {
                if (userID > 0)
                {
                    requestedUser = logic.FindUserByID(userID);
                }
                else
                {
                    requestedUser = user;
                }
            }
            catch (NotFoundException<IUser> ex)
            {
                BadRequest(ex.Message);
            }
            IProfile profile = null;
            try
            {
                profile = logic.GetProfileData(user, requestedUser, new ProfileFactory());
            }
            catch (NotSupportedActivityException ex)
            {
                BadRequest(ex.Message);
            }
            return profile;
        }

        //TODO DONE - Must write a logic function for updating profile. Parameter should be an IUser. ID from token must match with IUser.UserID for security concerns.
        //IUser must be handling the profile-data. Must not handling the new photos. 
        //New photos should have a different endpoints to handle the photo uploading, validating, and rendering to the user.
        [JWTCustomAuthorization]
        [HttpPost]
        [Route("api/user/updateprofile")]
        public IHttpActionResult UpdateProfile([FromBody] UserUpdateModel userProfile)
        {
            IUser user = GetUserFromContext();

            user.Age = userProfile.Age;
            user.Interested = userProfile.Interested;
            user.Male = userProfile.Male;
            user.MaxAge = userProfile.MaxAge;
            user.MinAge = userProfile.MinAge;
            user.Nickname = userProfile.UserName;
            user.WantToDate = false;
            /*
             * Should call logic.UpdateProfile: 
             *      the function should rerieve the existing user from the db by UserID and updating its values with this user. Then call UserRepo.Update.
             */
            if (userProfile.MinAge > userProfile.MaxAge)
            {
                return BadRequest("Minimum age must be less or equal to maximum age!");
            }
            try
            {
                logic.UpdateProfileData(user);
            }
            catch (NotFoundException<IUser> ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        //TODO DONE - This method must return a DateWrapper(MessageProfileWrapper) object containing the other user as a Profile abstract class, and a Collection for the messages.
        //Must get the requestor from the Auth Header and if it has a date, return that back as a DateWrapper. If not: call logic.GetDate to the user.
        [JWTCustomAuthorization]
        [HttpGet]
        [Route("api/user/date")]
        public HttpResponseMessage Date()
        {
            IUser user = GetUserFromContext();
            IUser date = null;
            try
            {
                date = logic.SearchForDate(user);
            }
            catch (DatePartnerNotFoundException ex)
            {
                try
                {
                    var newLogic = new Logic(this.messageLimit);
                    date = newLogic.SearchForDate(user);
                }
                catch(DatePartnerNotFoundException exNew)
                {
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, exNew.Message);
                }
            }

            Logic l = new Logic(this.messageLimit);
            IProfile profileData = l.GetProfileData(user, date, new ProfileFactory());
            ICollection<IMessage> messages = l.GetMessages(user, date);
            ICollection<IMessage> messageModels = DummyMessageModel.Convert(messages).ToList<IMessage>();
            ProfileMessageWrapper returnDate = new ProfileMessageWrapper(profileData, messageModels);
            
            return Request.CreateResponse(HttpStatusCode.OK, returnDate);
        }

        [JWTCustomAuthorization]
        [HttpPut]
        [Route("api/user/like/{userID:int}/{liked:bool}")]
        public IHttpActionResult Like(int userID, bool liked)
        {
            IUser userSender;
            IUser userReceiver;
            try
            {
                userSender = this.GetUserFromContext();
                userReceiver = logic.FindUserByID(userID);
            }
            catch (NotFoundException<IUser> ex)
            {
                return BadRequest("Request was invalid due to invalid user id." + ex.Message);
            }

            try
            {
                logic.AddLikeSecured(userSender, userReceiver, liked);
            }
            catch (NotSupportedActivityException ex)
            {

                return BadRequest(ex.Message);
            }

            return Ok();
        }

        
        [JWTCustomAuthorization]
        [HttpPut]
        [Route("api/user/sendmessagenew/{receiverID:int}")]
        public IHttpActionResult SendMessageNew(int receiverID) //return type should be: IHttpActionResult
        {
            IUser sender = GetUserFromContext();
            IUser receiver;
            IMessage returnMessage = null;

            try
            {
                receiver = logic.FindUserByID(receiverID);
            }
            catch (NotFoundException<IUser> ex)
            {
                return BadRequest(ex.Message);
            }

            var message = HttpContext.Current.Request.Params["message"];
            var photoReq = HttpContext.Current.Request.Params["photo"];
            var photo = HttpContext.Current.Request.Files.Get("photo");

            string imagePath = "";
            if (photo != null)
            {
                try
                {
                    imagePath = ImageProcesser.UploadPhoto(photo, sender.EMail);
                }
                catch (FormatException ex)
                {
                    return BadRequest("The provided photo was invalid: " + ex.Message);
                }
            }
            try
            {
                //this line should be where the photo is added too. The 1st part of the condition
                ContentType type;
                if (String.IsNullOrEmpty(message))
                {
                    if (!String.IsNullOrEmpty(imagePath))
                    {
                        type = ContentType.Photo;
                    }
                    else
                    {
                        type = ContentType.Both;
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(imagePath))
                    {
                        type = ContentType.Both;
                    }
                    else
                    {
                        type = ContentType.String;
                    }
                }
                returnMessage = logic.SendMessageSecure(sender, receiver, message, imagePath, type);

            }
            catch (FormatException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotSupportedActivityException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(new DummyMessageModel(returnMessage));
        }

        [JWTCustomAuthorization]
        [HttpPut]
        [Route("api/user/sendmessage/{receiverID:int}")]
        public IHttpActionResult SendMessage(int receiverID, [FromBody]string content)
        {
            IUser sender = GetUserFromContext();
            IUser receiver;
            IMessage message = null;
            try
            {
                receiver = logic.FindUserByID(receiverID);
            }
            catch (NotFoundException<IUser> ex)
            {
                return BadRequest(ex.Message);
            }

            var request = HttpContext.Current.Request;
            if (request.Files.Count > 0)
            {
                string imagePath = "";
                try
                {
                    imagePath = ImageProcesser.UploadPhoto(request, sender.EMail);
                    message = logic.SendMessageSecure(sender, receiver, content, imagePath, String.IsNullOrEmpty(content) ? ContentType.Photo : ContentType.Both);
                }
                catch (FormatException ex)
                {
                    return BadRequest(ex.Message);
                }
                catch (NotSupportedActivityException ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(content))
                {
                    return BadRequest("Message must contain some content (text or photo).");
                }
                try
                {
                    message = logic.SendMessageSecure(sender, receiver, content, String.Empty, ContentType.String);
                }
                catch (NotSupportedActivityException ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return Ok(new DummyMessageModel(message));
        }
        
        [JWTCustomAuthorization]
        [HttpPost]
        [Route("api/user/changepassword")]
        public IHttpActionResult ChangePassword([FromBody]UserPasswordChangeHolder credentials)
        {
            IUser user = GetUserFromContext();
            try
            {
                logic.ChangeUserPassword(user, credentials.PassW, credentials.NewPassword);
            }
            catch (WrongCredentialsException ex)
            {

                return BadRequest(ex.Message);
            }
            return Ok("Password changed successfully!");
        }
        
        [JWTCustomAuthorization]
        [HttpPost]
        [Route("api/user/changeemail")]
        public IHttpActionResult ChangeEmail([FromBody] UserEmailChangeHolder credentials)
        {
            IUser user = GetUserFromContext();
            try
            {
                logic.ChangeUserEmail(user, credentials.PassW, credentials.NewEmail);
            }
            catch (NotSupportedActivityException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (MultipleEmailException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (WrongCredentialsException ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok("E-mail changed successfully");
        }

        [JWTCustomAuthorization]
        [HttpGet]
        [Route("api/user/viewmatches")]
        public IHttpActionResult ViewMatches()
        {
            IUser user = GetUserFromContext();
            var matches = logic.LoadAllMatches(user);
            var dateData = logic.LoadDatePartnerData(user);
            Dictionary<Profile, string> matchesToReturn = new Dictionary<Profile, string>();
            

            foreach (var match in matches)
            {
                if (match.Value != null)
                {
                    matchesToReturn.Add(new FullProfile(match.Key), match.Value.ContentType == ContentType.Photo.ToString() ? $"{match.Key.Nickname} sent a photo." : match.Value.Content);
                }
                else
                {
                    matchesToReturn.Add(new FullProfile(match.Key), "No messages here....");
                }
            }

            List<FullProfile> profilesToReturn = new List<FullProfile>();
            foreach (var match in matches)
            {
                if (match.Value != null)
                {
                    profilesToReturn.Add(new FullProfile(match.Key, match.Value.ContentType == ContentType.Photo.ToString() ? $"{match.Key.Nickname} sent a photo." : match.Value.Content));
                }
                else
                {
                    profilesToReturn.Add(new FullProfile(match.Key, "No messages here...."));

                }
            }
            
            Dictionary<IProfile, IMessage> matchProfilesMessages = new Dictionary<IProfile, IMessage>();
            Dictionary<IProfile, IMessage> dateProfileMessages = new Dictionary<IProfile, IMessage>();
            foreach (var match in matches)
            {
                IProfile profile = logic.GetProfileData(user, match.Key, new ProfileFactory());
                matchProfilesMessages.Add(profile, match.Value != null ? new DummyMessageModel(match.Value) : null);
            }
            foreach (var d in dateData)
            {
                IProfile profile = logic.GetProfileData(user, d.Key, new ProfileFactory());
                dateProfileMessages.Add(profile, d.Value != null ? new DummyMessageModel(d.Value) : null);

            }
            MatchDateModel mDateModel = new MatchDateModel(matchProfilesMessages, dateProfileMessages);

            return Ok(mDateModel);
            
        }

        [JWTCustomAuthorization]
        [HttpGet]
        [Route("api/user/getunreadmessages/{userID:int}/{needToUpdate:bool}")]
        public IHttpActionResult GetUnreadMessages(int userID, bool needToUpdate)
        {
            IUser user = GetUserFromContext();
            IUser partner;

            IEnumerable<IMessage> messages;

            try
            {
                partner = logic.FindUserByID(userID);
            }
            catch(NotFoundException<IUser> ex)
            {
                return BadRequest(ex.Message);
            }

            try
            {
                messages = logic.GetUnreadMessages(user, partner);
            }
            catch (NotSupportedActivityException ex)
            {
                return BadRequest(ex.Message);
            }

            List<DummyMessageModel> models = new List<DummyMessageModel>();
            foreach (var message in messages)
            {
                models.Add(new DummyMessageModel(message));
            }
            if (needToUpdate)
            {
                logic.TurnMessageRead(messages.ToList(), user, partner); 
            }

            return Ok(models);
        }
        //TODO DONE - Retrieve the profile information and a collection of messages as ProfileMessageWrapper. This function could be used when retriving the date using the Date function.
        [JWTCustomAuthorization]
        [HttpGet]
        [Route("api/user/viewmessages/{userID:int}/{page:int}")]
        public IHttpActionResult ViewMessages(int userID, int page)
        {
            IUser user = GetUserFromContext();
            IUser partner;
            int messageNumber = 50;
            try
            {
                partner = logic.FindUserByID(userID);
            }
            catch (NotFoundException<IUser> ex)
            {
                return BadRequest(ex.Message);
            }
            ProfileMessageWrapper returnData = null;
            try
            {
                var messagesOriginal = logic.GetMessages(user, partner);
                int count = messagesOriginal.Count();
                IEnumerable<IMessage> messages;
                ICollection<IMessage> dummyMessages = new List<IMessage>();
                try
                {
                    int startIndex = page * messageNumber;
                    if (startIndex >= messagesOriginal.Count())
                    {
                        return BadRequest("There are no more messages to show...");
                    }
                    else
                    {
                        int countMessage;
                        if ((messagesOriginal.Count() - (startIndex + 1)) >= messageNumber)
                        {
                            countMessage = messageNumber;
                        }
                        else
                        {
                            countMessage = messagesOriginal.Count() - startIndex;
                        }

                        messages = messagesOriginal.Reverse().ToList().GetRange(startIndex, countMessage).Reverse<IMessage>();

                        foreach (var message in messages)
                        {
                            dummyMessages.Add(new DummyMessageModel(message));
                        }

                        if (messages.Where(x => x.Read == false).Where(x=>x.SenderID == partner.UserID).Count() > 0)
                        {
                            var unreadMessages = messages.Where(x => x.Read == false).Where(x=>x.SenderID == partner.UserID);
                            foreach (var message in unreadMessages)
                            {
                                logic.TurnMessageRead(message, user, partner);
                            }
                        }

                        return Ok(new ProfileMessageWrapper(logic.GetProfileData(user, partner, new ProfileFactory()), dummyMessages));
                    }

                   
                }
                catch (Exception ex)
                {

                    return BadRequest("Reached the start of the conversation.");
                }
            }
            catch (NotSupportedActivityException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Return an <see cref="IUser"/> from the database, based on the Authorization token from the Request header.
        /// </summary>
        /// <returns>The IUser of the request.</returns>
        /// <exception cref="NotFoundException{IUser}">Thrown when the user has not been found.</exception>
        private IUser GetUserFromContext()
        {
            int id = Convert.ToInt32(ActionContext.Request.Properties.Where(x => x.Key == "UserID").FirstOrDefault().Value);
            IUser user = null;
            try
            {
                user = logic.FindUserByID(id);
            }
            catch (NotFoundException<IUser> ex)
            {
                ActionContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            return user;
        }
        private int GetUserIDFromContext()
        {
            int id = Convert.ToInt32(ActionContext.Request.Properties.Where(x => x.Key == "UserID").FirstOrDefault().Value);
            return id;
        }

        //TODO - Testing the photo upload method!!! & Encrypt the folder name & photo name.
        //ImageProcesser class?
        [JWTCustomAuthorization]
        [HttpPost]
        [Route("api/user/profilephotouploaddeprecated/{location:int}")]
        public IHttpActionResult ProfilePhotoUploadDeprecated(int location)
        {
            //get the user from the authentication header
            IUser model = GetUserFromContext();

            int photoLocationMinBondary = 1;
            int photoLocationMaxBoundary = 8;

            //check if the file data is correct
            var request = HttpContext.Current.Request;
            if (request.Files.Count != 1)
            {
                return BadRequest("Only 1 file is enabled to be uploaded! Please specify only 1 file.");
            }
            if (!(location > (photoLocationMinBondary - 1) && location < (photoLocationMaxBoundary + 1)))
            {
                return BadRequest($"Wrong photo location! Please specify the location correctly: {photoLocationMinBondary} to {photoLocationMaxBoundary}");
            }

            //iterate thought the files in the request to save them 
            foreach (string file in request.Files)
            {
                var postedFile = request.Files[file];

                //get the folder name based on the Email adress of the user
                var lastDotIndex = model.EMail.LastIndexOf('.');
                var atSymbolIndex = model.EMail.LastIndexOf('@');
                char[] chars = model.EMail.ToArray();
                chars[lastDotIndex] = '_';
                chars[atSymbolIndex] = '_';

                var folderName = new string(chars);

                //If the directory doesnt exist, create one.
                /*
                if (!Directory.Exists("~/images/"+folderName))
                    Directory.CreateDirectory("~/images/"+folderName).;
                    */
                //Set the file prefix based on the requested url's location parameter
                string filePrefix = "";
                if (location < 6)
                {
                    filePrefix = "photo" + location + "-";
                }
                else
                {
                    filePrefix = "live" + location + "-";
                }

                //set the file name to generate unique files
                var fileName = filePrefix + DateTime.UtcNow + file;

                var filePath = HttpContext.Current.Server.MapPath("~/images/" + Path.Combine(folderName, fileName));
                postedFile.SaveAs(filePath);
            }
            return Ok("Upload completed!");
        }

        [JWTCustomAuthorization]
        [HttpPost]
        [Route("api/user/profilephotoupload/{location:int}")]
        public IHttpActionResult ProfilePhotoUpload(int location)
        {
            //get the user from the authentication header
            /*int id = GetUserIDFromContext();
            IUser model = logic.FindUserByID(id);
            */
            IUser user = GetUserFromContext();

            int photoLocationMinBondary = 1;
            int photoLocationMaxBoundary = 8;

            //check if the file data is correct
            var request = HttpContext.Current.Request;
            if (request.Files.Count != 1)
            {
                return BadRequest("Only 1 file is enabled to be uploaded! Please specify only 1 file.");
            }
            if (!(location > (photoLocationMinBondary - 1) && location < (photoLocationMaxBoundary + 1)))
            {
                return BadRequest($"Wrong photo location! Please specify the location correctly: {photoLocationMinBondary} to {photoLocationMaxBoundary}");
            }



            string filePrefix = "";
            if (photoLocationMinBondary > location || location > photoLocationMaxBoundary)
            {
                return BadRequest("Please provide a valid location [1-8].");

            }
            else
            {
                if (location < 6)
                {
                    filePrefix = "photo" + location + "-";
                }
                else
                {
                    filePrefix = "live" + location + "-";
                }
            }
            string photoPath = "";

            try
            {
                photoPath = ImageProcesser.UploadPhoto(request, user.EMail, filePrefix);
                logic.UploadPhoto(user, location, photoPath);
                return Ok(photoPath);
            }
            catch (FormatException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
