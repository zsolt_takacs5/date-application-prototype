﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Repository;
using AppInterfaces;
using DateAPI.Models;
using AppMutualInterfaces;

namespace DateAPI.Controllers
{
    public class ValuesController : ApiController
    {
        //public IUserRepository userRepo;
        //public ValuesController()
        //{
        //    userRepo = new UserRepository();
        //}
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public IUser Get(int id)
        {
            /*
            IUserRepository userRepo = new UserRepository();
            IUser model = new DummyUserModel(userRepo.FindUserByID(id));
            //return userRepo.UserFind(id);
            //return "value";
            //return userRepo.FindUserByID(id);
            return model;
            */
            return null;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
