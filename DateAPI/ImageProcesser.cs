﻿namespace DateAPI
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.IO;
    using System.Text;
    using System.Web;

    public static class ImageProcesser
    {
        //Source of this method: https://www.c-sharpcorner.com/article/resize-image-in-c-sharp/ 
        private static Image Resize(Image image, int newWidth, int maxHeight, bool onlyResizeIfWider)
        {
            if (onlyResizeIfWider && image.Width <= newWidth) newWidth = image.Width;

            var newHeight = image.Height * newWidth / image.Width;
            if (newHeight > maxHeight)
            {
                // Resize with height instead  
                newWidth = image.Width * maxHeight / image.Height;
                newHeight = maxHeight;
            }

            var res = new Bitmap(newWidth, newHeight);

            using (var graphic = Graphics.FromImage(res))
            {
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;
                graphic.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return res;
        }

        //Source: https://stackoverflow.com/questions/26353710/how-to-achieve-base64-url-safe-encoding-in-c
        private static string CreateBase64URL(string text)
        {
            string email = text;
            byte[] bytes = Encoding.ASCII.GetBytes(email);
            string folderName = Convert.ToBase64String(bytes).TrimEnd('=').Replace('+','-').Replace('/','_');
            return folderName;
        }
        private static string CreateTextFromBase64URL(string folderName)
        {
            string returnValue = folderName.Replace('_', '/').Replace('-', '+');
            switch (folderName.Length % 4)
            {
                case 2: returnValue += "=="; break;
                case 3: returnValue += "="; break;
            }
            byte[] bytes = Convert.FromBase64String(returnValue);
            string originalText = Encoding.ASCII.GetString(bytes);
            return originalText;
        }
        private static bool IsAllowedExtension(string extension)
        {
            switch (extension.ToLower())
            {
                case "jpg": return true;
                case "jpeg": return true;
                case "png": return true;
                default:
                    return false;
            }
        }
        public static string UploadPhoto(HttpPostedFile file, string email)
        {
            int maxFileSizeInBytes = 2 * 1000 * 1000;
            var fileArray = file.FileName.Split('.');
            var extension = fileArray[fileArray.Length - 1];
            //if (extension != "jpg" && extension != "jpeg") //De-morgan => IF not (jpg OR jpeg)
            //{
            //    throw new FormatException("Invalid picture format. Photos should be in a *.JPG or *.JPEG format!");
            //}
            if (!IsAllowedExtension(extension))
            {
                throw new FormatException("Invalid picture format. Photos should be in a *.JPG or *.JPEG format!");
            }
            if (file.ContentLength > maxFileSizeInBytes)
            {
                throw new FormatException($"Too big file! Photo must be less than {maxFileSizeInBytes / 1000 / 1000} MB (binary).");
            }

            //rename the file based on the uploader's data
            //var newFileNameInBytes = Encoding.UTF8.GetBytes(DateTime.UtcNow.ToString());

            //var newFileName = Convert.ToBase64String(newFileNameInBytes);
            var newFileName = CreateBase64URL(email + DateTime.UtcNow.ToString());
            var folderName = CreateBase64URL(email);
            if (!Directory.Exists(HttpContext.Current.Server.MapPath($"~/Photos/{folderName}")))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath($"~/Photos/{folderName}"));
            }
            var filePath = HttpContext.Current.Server.MapPath($"~/Photos/{folderName}/" + newFileName + "." + extension);
            file.SaveAs(filePath);

            var returnPath = $"/Photos/{folderName}/{newFileName}.{extension}";

            Image newImage = Image.FromFile(filePath);
            Image resizedImg = Resize(newImage, 480, 1500, true);
            newImage.Dispose();
            resizedImg.Save(filePath);
            resizedImg.Dispose();

            return returnPath;
    }

        /// <summary>
        /// Uploads a photo to the server and resizes it.
        /// The uploaded file will be placed in /Photos/{email in base64url encoded format}
        /// </summary>
        /// <param name="requestContext">The current request that holds the file.</param>
        /// <param name="email">The data to encrypt the photo name.</param>
        /// <returns>The path of the uploaded photo.</returns>
        /// <exception cref="FormatException">Thrown when there is a problem during uploading the file.</exception>
        public static string UploadPhoto(HttpRequest requestContext, string email)
        {
            var request = requestContext;
            var files = request.Files;
            int maxFileSizeInBytes = 2 * 1000 * 1000;
            if (files.Count == 1)
            {
                var postedFile = files[0];

                //get the name, extension of the file
                var fileArray = postedFile.FileName.Split('.');
                var extension = fileArray[fileArray.Length - 1];
                //if (extension != "jpg" && extension != "jpeg") //De-morgan => IF not (jpg OR jpeg)
                //{
                //    throw new FormatException("Invalid picture format. Photos should be in a *.JPG or *.JPEG format!");
                //}
                if (!IsAllowedExtension(extension))
                {
                    throw new FormatException("Invalid picture format. Photos should be in a *.JPG or *.JPEG format!");
                }
                if (postedFile.ContentLength > maxFileSizeInBytes)
                {
                    throw new FormatException($"Too big file! Photo must be less than {maxFileSizeInBytes / 1000 / 1000} MB (binary).");
                }

                //rename the file based on the uploader's data
                //var newFileNameInBytes = Encoding.UTF8.GetBytes(DateTime.UtcNow.ToString());

                //var newFileName = Convert.ToBase64String(newFileNameInBytes);
                var newFileName = CreateBase64URL(email + DateTime.UtcNow.ToString());
                var folderName = CreateBase64URL(email);
                if (!Directory.Exists(HttpContext.Current.Server.MapPath($"~/Photos/{folderName}")))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath($"~/Photos/{folderName}"));
                }
                var filePath = HttpContext.Current.Server.MapPath($"~/Photos/{folderName}/" + newFileName + "." + extension);
                postedFile.SaveAs(filePath);

                var returnPath = $"/Photos/{folderName}/{newFileName}.{extension}";

                Image newImage = Image.FromFile(filePath);
                Image resizedImg = Resize(newImage, 480, 1500, true);
                newImage.Dispose();
                resizedImg.Save(filePath);
                resizedImg.Dispose();
                return returnPath;
                
            }
            else
                throw new FormatException("Only 1 photo could be uploaded.");
        }

        public static void DeletePhoto(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            else
            {
                throw new FileNotFoundException("Photo can not found.", filePath);
            }
        }
        public static string UploadPhoto(HttpRequest requestContext, string email, string filePreFix)
        {
            var request = requestContext;
            var files = request.Files;
            int maxFileSizeInBytes = 2 * 1000 * 1000;
            if (files.Count == 1)
            {
                var postedFile = files[0];

                //get the name, extension of the file
                var fileArray = postedFile.FileName.Split('.');
                var extension = fileArray[fileArray.Length - 1];
                //if (extension != "jpg" && extension != "jpeg") //De-morgan => IF not (jpg OR jpeg)
                //{
                //    throw new FormatException("Invalid picture format. Photos should be in a *.JPG or *.JPEG format!");
                //}
                if (!IsAllowedExtension(extension))
                {
                    throw new FormatException("Invalid picture format. Photos should be in a *.JPG or *.JPEG format!");
                }
                if (postedFile.ContentLength > maxFileSizeInBytes)
                {
                    throw new FormatException($"Too big file! Photo must be less than {maxFileSizeInBytes / 1000 / 1000} MB (binary).");
                }

                //rename the file based on the uploader's data
                //var newFileNameInBytes = Encoding.UTF8.GetBytes(DateTime.UtcNow.ToString());

                //var newFileName = Convert.ToBase64String(newFileNameInBytes);
                var newFileName = filePreFix + CreateBase64URL(email + DateTime.UtcNow.ToString());
                var folderName = CreateBase64URL(email);
                if (!Directory.Exists(HttpContext.Current.Server.MapPath($"~/Photos/{folderName}")))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath($"~/Photos/{folderName}"));
                }
                var filePath = HttpContext.Current.Server.MapPath($"~/Photos/{folderName}/" + newFileName + "." + extension);
                postedFile.SaveAs(filePath);

                var returnPath = $"/Photos/{folderName}/{newFileName}.{extension}";

                Image newImage = Image.FromFile(filePath);
                Image resizedImg = Resize(newImage, 480, 1500, true);
                newImage.Dispose();
                resizedImg.Save(filePath);
                resizedImg.Dispose();
                return returnPath;

            }
            else
                throw new FormatException("Only 1 photo could be uploaded.");
        }
    }
}