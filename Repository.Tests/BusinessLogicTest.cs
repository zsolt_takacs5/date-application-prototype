﻿namespace BusinessLogic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NUnit.Framework;
    using Moq;
    using AppMutualInterfaces;
    using ApplicationPrototype;
    //using BusinessLogic;
    using DateAPI.Models;
    using System.Security.Cryptography;
    using Repository;
    using Repository.Exceptions;
    using BusinessLogic.Exceptions;

    [TestFixture]
    public class BusinessLogicTest
    {
        //NEW Logic test
        private USERS Andris, Bandi, Dezso, Elemer, Ferenc, Geza;
        private USERS Angela, Bea, Dia, Erika, Gabriella;
        private IUserRepository userRepo;
        private IMatchRepository matchRepo;
        private ILikeRepository likeRepo;
        private IMessageRepository messageRepo;
        private IDateRepository dateRepo;

        private Mock<IUserRepository> mockedUserRepo;
        private Mock<IMatchRepository> mockedMatchRepo;
        private Mock<ILikeRepository> mockedLikeRepo;
        private Mock<IMessageRepository> mockedMessageRepo;
        private Mock<IDateRepository> mockedDateRepo;

        private List<USERS> testUsers;
        private List<LIKES> testLikes;
        private List<MATCHES> testMatches;
        private List<USERMESSAGES> testMessages;
        private List<DATINGS> testDatings;

        private Logic bLogic;

        private int messageLimit;

        [SetUp]
        public void SetUp()
        {
            this.mockedUserRepo = new Mock<IUserRepository>();
            this.mockedLikeRepo = new Mock<ILikeRepository>();
            this.mockedMatchRepo = new Mock<IMatchRepository>();
            this.mockedMessageRepo = new Mock<IMessageRepository>();
            this.mockedDateRepo = new Mock<IDateRepository>();
            messageLimit = 50;

            bLogic = new Logic(this.mockedUserRepo.Object, this.mockedMatchRepo.Object, this.mockedLikeRepo.Object, this.mockedMessageRepo.Object, this.mockedDateRepo.Object, messageLimit);

            #region users
            //MALES---------------------------------------------------------
            Andris = new USERS()
            {
                UserID = 0,
                Age = 18,
                EMail = "andris18@gmail.com",
                Interested = false,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = true,
                MATCHES = new List<MATCHES>(),
                MaxAge = 25,
                MinAge = 18,
                Nickname = "Andris",
                PassW = "andrisJelszava",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Bandi = new USERS()
            {
                UserID = 1,
                Age = 20,
                EMail = "bandi20Csicskagyasz@gmail.com",
                Interested = false,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = true,
                MATCHES = new List<MATCHES>(),
                MaxAge = 22,
                MinAge = 19,
                Nickname = "Bandi",
                PassW = "BandiJelszo",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = null,
                Verified = false,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Dezso = new USERS()
            {
                UserID = 2,
                Age = 21,
                EMail = "dezso@gmail.com",
                Interested = false,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = true,
                MATCHES = new List<MATCHES>(),
                MaxAge = 35,
                MinAge = 20,
                Nickname = "Dezso",
                PassW = "dezsik3",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Elemer = new USERS()
            {
                UserID = 3,
                Age = 26,
                EMail = "elmi26@freemail.hu",
                Interested = false,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = true,
                MATCHES = new List<MATCHES>(),
                MaxAge = 27,
                MinAge = 22,
                Nickname = "Elemer",
                PassW = "3l3m3R",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Ferenc = new USERS()
            {
                UserID = 4,
                Age = 38,
                EMail = "ferkoka@gmail.com",
                Interested = false,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = true,
                MATCHES = new List<MATCHES>(),
                MaxAge = 55,
                MinAge = 34,
                Nickname = "Ferenc",
                PassW = "Fec0k@",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = false,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Geza = new USERS()
            {
                UserID = 5,
                Age = 51,
                EMail = "kerteszGeza@hotmail.com",
                Interested = false,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = true,
                MATCHES = new List<MATCHES>(),
                MaxAge = 25,
                MinAge = 18,
                Nickname = "Geza",
                PassW = "BananokKozt",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };

            //FEMALES--------------------------------------------------------
            Angela = new USERS()
            {
                UserID = 6,
                Age = 19,
                EMail = "ang1ka@gmail.com",
                Interested = true,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = false,
                MATCHES = new List<MATCHES>(),
                MaxAge = 25,
                MinAge = 17,
                Nickname = "Angela",
                PassW = "@ang3l!k@",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Bea = new USERS()
            {
                UserID = 7,
                Age = 27,
                EMail = "beacska@citromail.hu",
                Interested = true,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = false,
                MATCHES = new List<MATCHES>(),
                MaxAge = 35,
                MinAge = 27,
                Nickname = "Bea",
                PassW = "iLov3Gy0z0",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Dia = new USERS()
            {
                UserID = 8,
                Age = 23,
                EMail = "dianascukor@gmail.com",
                Interested = true,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = false,
                MATCHES = new List<MATCHES>(),
                MaxAge = 27,
                MinAge = 20,
                Nickname = "Dia",
                PassW = "dianascuki",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Erika = new USERS()
            {
                UserID = 9,
                Age = 50,
                EMail = "erika@freemail.hu",
                Interested = true,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = false,
                MATCHES = new List<MATCHES>(),
                MaxAge = 55,
                MinAge = 50,
                Nickname = "Erika",
                PassW = "Erik@50",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            Gabriella = new USERS()
            {
                UserID = 10,
                Age = 35,
                EMail = "gabika35@gmail.com",
                Interested = true,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = false,
                MATCHES = new List<MATCHES>(),
                MaxAge = 40,
                MinAge = 25,
                Nickname = "Gabriella",
                PassW = "Gab!ka35",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true,
                AlbumSharedBy = new List<USERS>(),
                AlbumSharedWith = new List<USERS>()
            };
            #endregion

            #region Assign Models to test DB

            testUsers = new List<USERS>()
            {
                Andris,
                Bandi,
                Dezso,
                Elemer,
                Ferenc,
                Geza,
                Angela,
                Bea,
                Dia,
                Erika,
                Gabriella
            };
            testLikes = new List<LIKES>();
            testMatches = new List<MATCHES>();
            testMessages = new List<USERMESSAGES>();
            testDatings = new List<DATINGS>();

            #endregion

            #region mock setup

            #region GetAll-setup

            this.mockedUserRepo.Setup(x => x.GetAll()).Returns(testUsers.AsQueryable());
            this.mockedLikeRepo.Setup(x => x.GetAll()).Returns(testLikes.AsQueryable());
            this.mockedMatchRepo.Setup(x => x.GetAll()).Returns(testMatches.AsQueryable());
            this.mockedMessageRepo.Setup(x => x.GetAll()).Returns(testMessages.AsQueryable());
            this.mockedDateRepo.Setup(x => x.GetAll()).Returns(testDatings.AsQueryable());

            #endregion

            #region UserRepo custom - setup
            this.mockedUserRepo.Setup(x => x.HashPassword(It.IsAny<string>())).Returns<string>((password) =>
             {
                 byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
                 SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();
                 string hash = Encoding.ASCII.GetString(provider.ComputeHash(passwordBytes)).Replace("-", "").ToLower();

                 return hash;
             });

            this.mockedUserRepo.Setup(x => x.VerifyHashedPassword(It.IsAny<string>(), It.IsAny<string>())).Returns<string, string>((hashedPassword, providedPassword) =>
              {
                  string providedHash = mockedUserRepo.Object.HashPassword(providedPassword);
                  return providedHash == hashedPassword;
              });

            this.mockedUserRepo.Setup(x => x.IsValidEmail(It.IsAny<string>())).Returns<string>((eMail) =>
              {
                  try
                  {
                      var addr = new System.Net.Mail.MailAddress(eMail);
                      return addr.Address == eMail;
                  }
                  catch
                  {
                      return false;
                  }
              });

            #endregion

            #region Create-setup

            this.mockedLikeRepo.Setup(x => x.Create(It.IsAny<LIKES>())).Callback<LIKES>((entity) => 
            {
                testLikes.Add(entity);
                testUsers.SingleOrDefault(x => x.EMail == entity.JudgeUser.EMail).Likes.Add(entity);
                testUsers.SingleOrDefault(x => x.EMail == entity.JudgedUser.EMail).LikedBy.Add(entity);
            });
            this.mockedMatchRepo.Setup(x => x.Create(It.IsAny<MATCHES>())).Callback<MATCHES>((entity) => 
            {
                if (testMatches.Count > 0)
                {
                    entity.MatchID = testMatches.Last().MatchID + 1;
                }
                else
                {
                    entity.MatchID = testMatches.Count + 1;
                }
                testMatches.Add(entity);
                testUsers.SingleOrDefault(x => x.EMail == entity.USER.EMail).MATCHES.Add(entity);
            });
            this.mockedMessageRepo.Setup(x => x.Create(It.IsAny<USERMESSAGES>())).Callback<USERMESSAGES>((entity) => 
            {
                entity.MessageID = testMessages.Count + 1;
                testMessages.Add(entity);
                var user = testUsers.SingleOrDefault(x => x.UserID == entity.SenderID);
                user.USERMESSAGES.Add(entity);
                entity.USERS = user;
                entity.SenderID = user.UserID;
                if (entity.Match != null)
                {
                    var match = testMatches.SingleOrDefault(x => x.MatchID == entity.MatchID);
                    match.USERMESSAGES.Add(entity);
                    entity.Match = match; 
                }
                else if(entity.MatchID != null)
                {
                    var match = testMatches.SingleOrDefault(x => x.MatchID == entity.MatchID);
                    match.USERMESSAGES.Add(entity);
                    entity.Match = match;
                }
                if(entity.Date != null)
                {
                    var date = testDatings.SingleOrDefault(x => x.DateID == entity.DateID);
                    date.USERMESSAGES.Add(entity);
                    entity.Date = date;
                }
            });
            this.mockedUserRepo.Setup(x => x.Create(It.IsAny<USERS>())).Callback<USERS>((entity) => 
            {
                if (this.mockedUserRepo.Object.FindUserByEmail(entity.EMail) != null)
                    throw new MultipleEmailException("multiple mail mothefucka");
                else
                {
                    entity.UserID = testUsers.Count() + 1;
                    testUsers.Add(entity);
                }
            });
            this.mockedDateRepo.Setup(x => x.Create(It.IsAny<DATINGS>())).Callback<DATINGS>((entity) =>
              {
                  testDatings.Add(entity);
                  foreach (USERS user in entity.USERS)
                  {
                      user.Date = entity;
                  }
              });

            #endregion

            #region Update-setup

            this.mockedLikeRepo.Setup(x => x.Update(It.IsAny<LIKES>())).Callback<LIKES>((entity) => 
            {
                var like = testLikes.SingleOrDefault(x => x.Judge == entity.Judge && x.JudgedPerson == x.JudgedPerson);
                if (like != null)
                {
                    like.Liked = entity.Liked;
                }
            });
            this.mockedMatchRepo.Setup(x => x.Update(It.IsAny<MATCHES>())).Callback<MATCHES>((entity) => 
            {
                var match = testMatches.SingleOrDefault(x=>x.MatchID == entity.MatchID);
                if (match != null)
                {
                    match.MatchDate = entity.MatchDate;
                    match.USER = entity.USER;
                    match.User1_ID = entity.User1_ID;
                    match.User2_ID = entity.User2_ID;
                    match.USERMESSAGES = entity.USERMESSAGES;
                    match.USER = entity.USER; 
                }
            });
            this.mockedMessageRepo.Setup(x => x.Update(It.IsAny<USERMESSAGES>())).Callback<USERMESSAGES>((entity) => 
            {
                var message = testMessages.SingleOrDefault(x => x.MessageID == entity.MessageID);
                if (message != null)
                {
                    message.Content = entity.Content;
                    message.ContentPhoto = entity.ContentPhoto;
                    message.ContentType = entity.ContentType;
                    message.Match = entity.Match;
                    message.MatchID = entity.MatchID;
                    message.MessageDate = entity.MessageDate;
                    message.SenderID = entity.SenderID;
                    message.USERS = entity.USERS;
                }
            });
            this.mockedUserRepo.Setup(x => x.Update(It.IsAny<USERS>())).Callback<USERS>((entity) =>
              {
                  var user = testUsers.SingleOrDefault(x => x.UserID == entity.UserID);
                  if (user != null)
                  {
                      user.Age = entity.Age;
                      user.Nickname = entity.Nickname;
                      user.EMail = entity.EMail;
                      user.PassW = entity.PassW;
                      user.Interested = entity.Interested;
                      user.Male = entity.Male;
                      user.Live1 = entity.Live1;
                      user.Live2 = entity.Live2;
                      user.Live3 = entity.Live3;
                      user.Photo1 = entity.Photo1;
                      user.Photo2 = entity.Photo2;
                      user.Photo3 = entity.Photo3;
                      user.Photo4 = entity.Photo4;
                      user.Photo5 = entity.Photo5;
                      user.SubscriptionDate = entity.SubscriptionDate;
                      user.SubscriptionLevel = entity.SubscriptionLevel;
                      user.Verified = entity.Verified;
                      user.WantToDate = entity.WantToDate;
                      user.USERMESSAGES = entity.USERMESSAGES;
                      user.MATCHES = entity.MATCHES;
                      user.LikedBy = entity.LikedBy;
                      user.Likes = entity.Likes;
                  }
              });
            this.mockedDateRepo.Setup(x => x.Update(It.IsAny<DATINGS>())).Callback<DATINGS>((entity) =>
            {
                var date = testDatings.SingleOrDefault(x => x.DateID == entity.DateID);
                if (date != null)
                {
                    date.USERMESSAGES = entity.USERMESSAGES;
                    date.USERS = entity.USERS;
                }
            });

            #endregion

            #region Delete-setup

            this.mockedLikeRepo.Setup(x => x.Delete(It.IsAny<LIKES>())).Callback<LIKES>((entity) => 
            {
                var like = testLikes.SingleOrDefault(x => x.Judge == entity.Judge && x.JudgedPerson == entity.JudgedPerson);
                if (like!= null)
                {
                    testLikes.Remove(entity);
                }
            });
            this.mockedMatchRepo.Setup(x => x.Delete(It.IsAny<MATCHES>())).Callback<MATCHES>((entity) => 
            {
                var match = testMatches.SingleOrDefault(x => x.MatchID == entity.MatchID);
                if (match != null)
                {
                    testMatches.Remove(match);
                }
            });
            this.mockedMessageRepo.Setup(x => x.Delete(It.IsAny<USERMESSAGES>())).Callback<USERMESSAGES>((entity) =>
              {
                  var message = testMessages.SingleOrDefault(x => x.MessageID == entity.MessageID);
                  if (message != null)
                  {
                      testMessages.Remove(message);
                  }
              });
            this.mockedDateRepo.Setup(x => x.Delete(It.IsAny<DATINGS>())).Callback<DATINGS>((entity) =>
              {
                  var date = testDatings.SingleOrDefault(x => x.DateID == entity.DateID);
                  if (date != null)
                  {
                      testDatings.Remove(date);
                  }
              });

            #endregion

            #region Repository specific Find methods

            #region Like Repository

            //FindByUsers
            this.mockedLikeRepo.Setup(x => x.FindByUsers(It.IsAny<IUser>(), It.IsAny<IUser>())).Returns<IUser, IUser>((judge, judged) => 
            {
                var like = testLikes.SingleOrDefault(x => x.JudgeUser.UserID == judge.UserID && x.JudgedUser.UserID == judged.UserID);
                return like;
            });

            //FindAllLikesUserGive
            this.mockedLikeRepo.Setup(x => x.FindAllLikesUserGive(It.IsAny<IUser>())).Returns<IUser>((user) => 
            {
                var likes = testLikes.Where(x => x.Judge == user.UserID);
                return likes.ToList();
            });

            //FindAllLikesUserGot
            this.mockedLikeRepo.Setup(x => x.FindAllLikesUserGot(It.IsAny<IUser>())).Returns<IUser>((user) => 
            {
                var likes = testLikes.Where(x => x.JudgedPerson == user.UserID);
                return likes.ToList();
            });
            this.mockedLikeRepo.Setup(x => x.FindAllLikesAssignedToUser(It.IsAny<IUser>())).Returns<IUser>((user) =>
              {
                  return this.mockedLikeRepo.Object.FindAllLikesUserGot(user).Union(this.mockedLikeRepo.Object.FindAllLikesUserGive(user)).ToList();
              });
            #endregion

            #region Match Repository

            //FindByID
            this.mockedMatchRepo.Setup(x => x.FindById(It.IsAny<int>())).Returns<int>((ID) => 
            {
                return this.mockedMatchRepo.Object.GetAll().Where(x => x.MatchID == ID).FirstOrDefault();
            });

            //FindByUsers
            this.mockedMatchRepo.Setup(x => x.FindByUsers(It.IsAny<IUser>(), It.IsAny<IUser>())).Returns<IUser, IUser>((userA, userB) =>
              {
                  var matches = this.mockedMatchRepo.Object.GetAll();
                  var m1 = matches.Where(x => x.User1_ID == userA.UserID && x.User2_ID == userB.UserID);
                  return m1.FirstOrDefault();
              });

            #endregion

            #region Message Repository

            //FindById
            this.mockedMessageRepo.Setup(x => x.FindByID(It.IsAny<int>())).Returns<int>((ID) =>
              {
                  return this.mockedMessageRepo.Object.GetAll().SingleOrDefault(x => x.MessageID == ID);
              });

            //FindByUserSent
            this.mockedMessageRepo.Setup(x => x.FindByUserSent(It.IsAny<IUser>())).Returns<IUser>((user) =>
              {
                  return this.mockedMessageRepo.Object.GetAll().Where(x => x.SenderID == user.UserID).ToList();
              });

            //FindByMatch
            this.mockedMessageRepo.Setup(x => x.FindByMatch(It.IsAny<IMatch>())).Returns<IMatch>((match) =>
              {
                  return this.mockedMessageRepo.Object.GetAll().Where(x => x.MatchID == match.MatchID).ToList();
              });

            #endregion

            #region User Repository

            //FindUserById
            this.mockedUserRepo.Setup(x => x.FindByID(It.IsAny<int>())).Returns<int>((ID) =>
              {
                  return this.mockedUserRepo.Object.GetAll().Where(x => x.UserID == ID).FirstOrDefault();
              });

            //FindUserByEmail
            this.mockedUserRepo.Setup(x => x.FindUserByEmail(It.IsAny<string>())).Returns<string>((Email) =>
            {
                return this.mockedUserRepo.Object.GetAll().Where(x => x.EMail == Email).FirstOrDefault();
            });
            #endregion

            #region Date Repository
            this.mockedDateRepo.Setup(x => x.FindByID(It.IsAny<int>())).Returns<int>((ID) =>
            {
                return this.mockedDateRepo.Object.GetAll().Where(x => x.DateID == ID).FirstOrDefault();
            });
            #endregion

            #endregion


            #endregion
        }
        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void SearchForPartner_ReturnsTheCorrectPartner(string userName, string partnerName)
        {
            //Arrange - Act
            var users = mockedUserRepo.Object.GetAll();
            var userNeedsPartner = users.Single(x => x.Nickname == userName);
            bLogic.SearchForDate(userNeedsPartner);

            var partner = userNeedsPartner.Date.USERS.Last();
            
            //Assert
            Assert.That(partner.Nickname, Is.EqualTo(partnerName));
        }

        private void CreateDate_And_SendMessages(string userName1, string userName2, int messageNumber)
        {
            var user1 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName2);

            DATINGS date = new DATINGS()
            {
                DateID = 1,
                USERMESSAGES = new List<USERMESSAGES>(),
                USERS = new List<USERS>() { user1, user2 }
            };
            mockedDateRepo.Object.Create(date);

            for (int i = 0; i < messageNumber+1; i++)
            {
                USERMESSAGES message = new USERMESSAGES()
                {
                    Content = "test",
                    ContentPhoto = string.Empty,
                    ContentType = "string",
                    Date = date,
                    DateID = date.DateID,
                    Match = null,
                    MatchID = null,
                    MessageDate = DateTime.Now,
                    MessageID = i,
                    SenderID = user1.UserID,
                    USERS = user1
                };
                mockedMessageRepo.Object.Create(message);
            }
        }

        [TestCase("Andris", "Angela")]
        public void SearchForPartner_PutThemInDate_UsersCountIsCorrect(string userName, string partnerName)
        {
            //Arrange
            var users = mockedUserRepo.Object.GetAll();
            var userNeedsPartner = users.Single(x => x.Nickname == userName);
            var partner = users.Single(x => x.Nickname == partnerName);

            //ACT
            bLogic.SearchForDate(userNeedsPartner);

            //Assert
            var date = mockedDateRepo.Object.GetAll().FirstOrDefault();
            List<USERS> usersToCompare = new List<USERS>() { userNeedsPartner, partner };

            Assert.That(date.USERS.Count(), Is.EqualTo(2));
            //Assert.That(userNeedsPartner.Date.USERS.Contains(userNeedsPartner), Is.EqualTo(true));
        }
        [TestCase("Andris", "Angela")]
        public void SearchForPartner_PutThemInDate_BothIsInTheSameDate(string userName, string partnerName)
        {
            //Arrange
            var users = mockedUserRepo.Object.GetAll();
            var userNeedsPartner = users.Single(x => x.Nickname == userName);
            var partner = users.Single(x => x.Nickname == partnerName);

            //ACT
            bLogic.SearchForDate(userNeedsPartner);

            //Assert
            var date = mockedDateRepo.Object.GetAll().FirstOrDefault();
            List<USERS> usersToCompare = new List<USERS>() { userNeedsPartner, partner };

            Assert.That(date.USERS, Is.EqualTo(usersToCompare));
            //Assert.That(userNeedsPartner.Date.USERS.Contains(userNeedsPartner), Is.EqualTo(true));
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Erika", "Ferenc")]
        public void BothLike_EndUp_WithMatch(string senderName, string receiverName)
        {
            var users = mockedUserRepo.Object.GetAll();
            var sender = users.Single(x => x.Nickname == senderName);
            var receiver = users.Single(x => x.Nickname == receiverName);

            CreateDate_And_SendMessages(senderName, receiverName, 50);

            bLogic.AddLike(sender, receiver, true);
            bLogic.AddLike(receiver, sender, true);

            var Matchername = mockedUserRepo.Object.FindByID(sender.MATCHES.First().User2_ID).Nickname;

            Assert.That(sender.Likes.Count(), Is.EqualTo(1));
            Assert.That(receiver.Likes.Count(), Is.EqualTo(1));
            Assert.That(sender.MATCHES.Count(), Is.EqualTo(1));
            Assert.That(receiver.MATCHES.Count(), Is.EqualTo(1));
            Assert.That(Matchername, Is.EqualTo(receiverName));
            Assert.That(testMatches.Count(), Is.EqualTo(2));
        }

        [TestCase("Andris", "Dia", "Erika")]
        public void ReturnMatches_Returns_CorrectUser(string userMale, string userFemale1, string userFemale2)
        {
            var male = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userMale);
            var female1 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userFemale1);
            var female2 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userFemale2);

            bLogic.AddLike(male, female1, true);
            bLogic.AddLike(male, female2, true);
            bLogic.AddLike(female1, male, true);
            bLogic.AddLike(female2, male, true);


            Assert.That(bLogic.LoadMatches(male).First().Nickname, Is.EqualTo(userFemale1));
        }

        [TestCase("Elemer", "Bea")]
        [TestCase("Andris", "Angela")]
        public void SendMessage_ThroughMatch_Successfully_Content_Is_Right(string senderName, string receiverName)
        {
            //Arrange
            var sender = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == senderName);
            var receiver = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName);

            CreateDate_And_SendMessages(senderName, receiverName, messageLimit);

            bLogic.AddLike(sender, receiver, true);
            bLogic.AddLike(receiver, sender, true);

            var match = sender.MATCHES.FirstOrDefault();

            //Act
            bLogic.SendMessage(sender, match, "TestMessage to: " + receiver.Nickname, string.Empty, ContentType.String);

            //Assert
            //Message content is right?
            Assert.That(sender.USERMESSAGES.ElementAt(sender.USERMESSAGES.Count - 2).Content, Is.EqualTo("TestMessage to: " + receiver.Nickname));
            //other user of the match is the same user who is recieving the message?
            //Assert.That(sender.USERMESSAGES.FirstOrDefault().Match.User2_ID, Is.EqualTo(otherUser.UserID));
        }
        
        [TestCase("testMale68", "test", 68)]
        public void UpdateProfile_Successfull(string name, string pw, int newAge)
        {
            ICredentialable userCredentials = new UserCredentials();
            userCredentials.EMail = name + "@gmail.com";
            userCredentials.PassW = pw;

            DummyRegisterModel model = new DummyRegisterModel(true, true, 20, 26, 25, name, name + "@gmail.com", pw);
            bLogic.RegisterUser(model, "photo");
            IUser user = bLogic.FindUserByEmail(name+"@gmail.com");
            user.Age = newAge;
            bLogic.UpdateProfileData(user);

            Assert.That(bLogic.FindUserByEmail(name+"@gmail.com").Age, Is.EqualTo(newAge));
        }

        [TestCase("Elemer", "Bea")]
        [TestCase("Andris", "Angela")]
        public void SendMessage_ThroughMatch_Receiver_IsTheSame_As_TheOtherUserOfTheMatch(string senderName, string receiverName)
        {
            //Arrange
            var sender = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == senderName);
            var receiver = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName);

            CreateDate_And_SendMessages(senderName, receiverName, messageLimit);

            bLogic.AddLike(sender, receiver, true);
            bLogic.AddLike(receiver, sender, true);
            

            var match = sender.MATCHES.FirstOrDefault();

            //Act
            var message = bLogic.SendMessage(sender, match, "TestMessage to: " + receiver.Nickname, "test", ContentType.String);

            //Assert
            //other user of the match is the same user who is recieving the message?
            Assert.That(sender.USERMESSAGES.ElementAt(sender.USERMESSAGES.Count - 2).Match.User2_ID, Is.EqualTo(receiver.UserID));
        }

        [TestCase("Elemer", "Bea", 5)]
        [TestCase("Andris", "Angela", 10)]
        public void SendMultipleMessages_ThroughMatch_UsersMessagesCount_IsCorrect(string senderName, string receiverName, int numberOfMessages)
        {
            var sender = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == senderName);
            var receiver = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName);

            //CreateDate_And_SendMessages(senderName, receiverName, messageLimit);

            bLogic.AddLike(sender, receiver, true);
            bLogic.AddLike(receiver, sender, true);

            var match = mockedMatchRepo.Object.FindByUsers(sender, receiver);

            for (int i = 0; i < numberOfMessages; i++)
            {
                bLogic.SendMessage(sender, match, "TestMessage to: " + receiver.Nickname, string.Empty, ContentType.String);
            }

            //Message count is right?
            Assert.That(sender.USERMESSAGES.Count, Is.EqualTo(numberOfMessages*2));
        }

        [TestCase("Andris", "Angela")]
        public void CountOfLikes_Sender_IsCorrect(string senderName, string receiverName)
        {
            var sender = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == senderName);
            var receiver = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName);

            bLogic.AddLike(sender, receiver, true);

            Assert.That(sender.Likes.Count(), Is.EqualTo(1));
        }

        [TestCase("Andris", "Angela", "Dia", "Gabriella")]
        public void Count_Of_MultipleLikes_IsCorrect(string senderName, string receiverName1, string receiverName2, string receiverName3)
        {
            var sender = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == senderName);
            var receiver = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName1);
            var receiver2 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName2);
            var receiver3 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName3);

            bLogic.AddLike(sender, receiver, true);
            bLogic.AddLike(sender, receiver2, true);
            bLogic.AddLike(sender, receiver3, true);

            Assert.That(sender.Likes.Count(), Is.EqualTo(3));
        }

        [TestCase("Andris", "Angela", "Dia", "Gabriella")]
        public void Count_Of_MultipleLikes_MiddleUser_IsCorrect(string senderName, string receiverName1, string receiverName2, string receiverName3)
        {
            var sender = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == senderName);
            var receiver = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName1);
            var receiver2 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName2);
            var receiver3 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == receiverName3);

            bLogic.AddLike(sender, receiver, true);
            bLogic.AddLike(sender, receiver2, true);
            bLogic.AddLike(sender, receiver3, true);

            Assert.That(sender.Likes.ElementAt(1).JudgedUser.Nickname, Is.EqualTo(receiver2.Nickname));
        }

        [TestCase("bandi20Csicskagyasz@gmail.com")]
        [TestCase("ang1ka@gmail.com")]
        [TestCase("ferkoka@gmail.com")]
        public void Register_User_Email_Used_Throws_MultipleEmailException(string email)
        {
            USERS registerUser = new USERS()
            {
                UserID = 11,
                Age = 18,
                EMail = email,
                Interested = false,
                LikedBy = new List<LIKES>(),
                Likes = new List<LIKES>(),
                Live1 = "livePic1",
                Live2 = "LivePic2",
                Live3 = "LivePic3",
                Male = true,
                MATCHES = new List<MATCHES>(),
                MaxAge = 25,
                MinAge = 18,
                Nickname = "newUser",
                PassW = "password",
                Photo1 = "photo1",
                Photo2 = "photo2",
                Photo3 = "photo3",
                Photo4 = "photo4",
                Photo5 = "photo5",
                RegisteredDate = DateTime.Now,
                SubscriptionDate = DateTime.Now,
                SubscriptionLevel = 0,
                USERMESSAGES = new List<USERMESSAGES>(),
                Verified = true,
                WantToDate = true
            };
            DummyRegisterModel model = new DummyRegisterModel(registerUser.Male, registerUser.Interested, registerUser.MinAge, registerUser.MinAge, registerUser.Age, registerUser.Nickname, registerUser.EMail, registerUser.PassW);

            Assert.Throws<MultipleEmailException>(() => bLogic.RegisterUser(model, null));
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void AddLike_MessageLimitReached(string userName1, string userName2)
        {
            //arrange
            CreateDate_And_SendMessages(userName1, userName2, messageLimit + 1);
            var user1 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName2);

            //act
            bLogic.AddLikeSecured(user1, user2, true);

            //assert
            var like = mockedLikeRepo.Object.GetAll().FirstOrDefault();
            Assert.That((mockedLikeRepo.Object.GetAll().Count() == 1)
                && (like.Judge == user1.UserID)
                && (like.JudgedPerson == user2.UserID), Is.EqualTo(true));
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void AddDislike_MessageLimitNotReached(string userName1, string userName2)
        {
            //arrange
            CreateDate_And_SendMessages(userName1, userName2, 0);
            var user1 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName2);

            //act
            bLogic.AddLikeSecured(user1, user2, false);

            //assert
            var like = mockedLikeRepo.Object.GetAll().FirstOrDefault();
            Assert.That((mockedLikeRepo.Object.GetAll().Count() == 1)
                && (like.Judge == user1.UserID)
                && (like.JudgedPerson == user2.UserID)
                && (like.Liked == false), Is.EqualTo(true));
        }
        
        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void AddLike_Users_Not_Dating_Throws_Exception(string userName1, string userName2)
        {
            //arrange
            var user1 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName2);

            //act - assert
            Assert.That(() => bLogic.AddLikeSecured(user1, user2, true), Throws.Exception.TypeOf<NotSupportedActivityException>());
            
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void AddLike_Users_Not_Related_Throws_Exception(string userName1, string userName2)
        {
            //arrange
            var user1 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName2);

            //act-arrange
            Assert.That(() => bLogic.AddLikeSecured(user1, user2, true), Throws.Exception.TypeOf<NotSupportedActivityException>());
        }

        [TestCase("123@mail.com", "testing", "testing1", "testing")]
        [TestCase("321@mail.com", "testingMail", "testing1", "testingMail")]
        [TestCase("foursixseven@mail.com", "testPassword", "testing1", "testPassword")]
        public void ChangePassword_Provide_Correct_Password(string eMail, string password, string newPassword, string existingPassword)
        {
            //Arrange: register user with details
            ICredentialable userCredentials = new UserCredentials()
            {
                EMail = eMail,
                PassW = password
            };
            DummyRegisterModel model = new DummyRegisterModel(true, true, 20, 27, 25, "name", eMail, password);

            bLogic.RegisterUser(model, "photo");

            //Act: change user password
            var user = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.EMail == eMail);
            bLogic.ChangeUserPassword(user, existingPassword, newPassword);

            //Assert: check if new password update has done...
            Assert.That(mockedUserRepo.Object.VerifyHashedPassword(user.PassW, newPassword), Is.EqualTo(true));
        }
        
        [TestCase("test123@mail.com", "testPassword", "ang1ka@gmail.com")]
        public void Change_Email_Provide_Existing_Email_Throws_Exception(string eMail, string password, string newEmail)
        {
            //arrange
            ICredentialable userCredentials = new UserCredentials()
            {
                EMail = eMail,
                PassW = password
            };
            DummyRegisterModel model = new DummyRegisterModel(true, true, 20, 27, 25, "name", eMail, password);
            bLogic.RegisterUser(model, "photo");

            //act-assert
            var user = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.EMail == eMail);
            Assert.That(() => bLogic.ChangeUserEmail(user, password, newEmail), Throws.Exception.TypeOf<MultipleEmailException>());
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void ShareAlbum_Message_Limit_Reached(string userName1, string userName2)
        {
            //arrange
            CreateDate_And_SendMessages(userName1, userName2, messageLimit + 1);
            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            //act
            bLogic.AlbumShare(user1, user2);
            Assert.That(user1.AlbumSharedWith.Contains(user2)
                && user1.AlbumSharedWith.Count == 1, Is.EqualTo(true));
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void ShareAlbum_Message_Limit_Not_Reached_Throws_Exception(string userName1, string userName2)
        {
            //arrange
            CreateDate_And_SendMessages(userName1, userName2, 1);
            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            //act - assert
            Assert.That(() => bLogic.AlbumShareSecured(user1, user2), Throws.Exception.TypeOf<NotSupportedActivityException>());
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void ShareAlbum_Users_Matched_Throws_Exception(string userName1, string userName2)
        {
            //arrange
            CreateDate_And_SendMessages(userName1, userName2, messageLimit + 1);
            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            bLogic.AddLikeSecured(user1, user2, true);
            bLogic.AddLikeSecured(user2, user1, true);

            //act-assert
            Assert.That(() => bLogic.AlbumShareSecured(user1, user2), Throws.Exception.TypeOf<NotSupportedActivityException>());
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void ShareAlbum_Users_Not_Related_Throws_Exception(string userName1, string userName2)
        {
            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            Assert.That(() => bLogic.AlbumShareSecured(user1, user2), Throws.Exception.TypeOf<NotSupportedActivityException>());
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void ShareAlbum_Users_Dating_Already_Shared_Throws_Exception(string userName1, string userName2)
        {
            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            CreateDate_And_SendMessages(userName1, userName2, messageLimit + 10);

            bLogic.AlbumShare(user1, user2);

            Assert.That(() => bLogic.AlbumShareSecured(user1, user2), Throws.Exception.TypeOf<NotSupportedActivityException>());
        }

        [TestCase("Andris", "Angela", true)]
        [TestCase("Angela", "Andris", true)]
        [TestCase("Dezso", "Dia", false)]
        [TestCase("Gabriella", "Ferenc", false)]
        public void GetProfile_Dating_MessageLimit_Reached_Returns_Correct_AbilitiesCount(string userName1, string userName2, bool albumShared)
        {
            CreateDate_And_SendMessages(userName1, userName2, messageLimit + 10);

            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            if (albumShared)
            {
                bLogic.AlbumShare(user2, user1);
                user1.AlbumSharedBy.Add(user2);
                var profile = bLogic.GetProfileData(user1, user2, new ProfileFactory());
                Assert.That(profile.Abilities.Count, Is.EqualTo(3));
                //Assert.That(profile.Abilities.Count, Is.EqualTo(1));

            }
            else
            {
                var profile = bLogic.GetProfileData(user1, user2, new ProfileFactory());
                Assert.That(profile.Abilities.Count, Is.EqualTo(3));
                //Assert.That(profile.Abilities.Count, Is.EqualTo(3));

            }
        }

        [TestCase("Andris", "Angela", true)]
        [TestCase("Angela", "Andris", true)]
        [TestCase("Dezso", "Dia", false)]
        [TestCase("Gabriella", "Ferenc", false)]
        public void GetProfile_Dating_MessageLimit_Reached_Returns_Correct_Abilities(string userName1, string userName2, bool albumShared)
        {
            CreateDate_And_SendMessages(userName1, userName2, messageLimit + 10);

            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            if (albumShared)
            {
                bLogic.AlbumShare(user2, user1);
                user1.AlbumSharedBy.Add(user2);
                var profile = bLogic.GetProfileData(user2, user1, new ProfileFactory());
                Assert.That(profile.Abilities.Where(x=>x.Value == true).Count, Is.EqualTo(2));
                //Assert.That(profile.Abilities.Count, Is.EqualTo(1));

            }
            else
            {
                var profile = bLogic.GetProfileData(user1, user2, new ProfileFactory());
                Assert.That(profile.Abilities.Where(x => x.Value == true).Count, Is.EqualTo(3));
                //Assert.That(profile.Abilities.Count, Is.EqualTo(3));

            }
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void GetProfile_Dating_MessageLimit_Not_Reached_Returns_Zero_Abilities(string userName1, string userName2)
        {
            CreateDate_And_SendMessages(userName1, userName2, 0);

            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            var profile = bLogic.GetProfileData(user1, user2, new ProfileFactory());
            Assert.That(profile.Abilities.Count, Is.EqualTo(3));
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void GetProfile_Dating_MessageLimit_Not_Reached_Abilities_Contains_False_ForLike(string userName1, string userName2)
        {
            CreateDate_And_SendMessages(userName1, userName2, 0);

            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            var profile = bLogic.GetProfileData(user1, user2, new ProfileFactory());
            Assert.That(profile.Abilities["LikeAble"], Is.EqualTo(false));
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void GetProfile_Dating_MessageLimit_Not_Reached_Abilities_Contains_False_ForShare(string userName1, string userName2)
        {
            CreateDate_And_SendMessages(userName1, userName2, 0);

            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            var profile = bLogic.GetProfileData(user1, user2, new ProfileFactory());
            Assert.That(profile.Abilities["ShareAlbum"], Is.EqualTo(false));
        }

        [TestCase("Andris", "Angela")]
        [TestCase("Angela", "Andris")]
        [TestCase("Dezso", "Dia")]
        [TestCase("Gabriella", "Ferenc")]
        public void AddLike_MessageLimitNotReached_ThrowsException(string userName1, string userName2)
        {
            //arrange
            CreateDate_And_SendMessages(userName1, userName2, 0);
            var user1 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName1);
            var user2 = mockedUserRepo.Object.GetAll().SingleOrDefault(x => x.Nickname == userName2);

            Assert.That(() => bLogic.AddLikeSecured(user1, user2, true), Throws.Exception.TypeOf<NotSupportedActivityException>());
        }

        /*
        [TestCase("Andris")]
        public void SearchForPartner_DoesntReturnNull_WhenItShouldReturnUser(string userName)
        {
            var userNeedsPartner = mockedUserRepo.Object.GetAll().Single(x => x.Nickname == userName);
            //var partner = Repo.SearchForDate(userNeedsPartner, mockedUserRepo.Object.GetAll());
            var partner = Repo.SearchForDate(userNeedsPartner);
            Assert.That(partner, Is.Not.Null);
        }
        [TestCase("Elemer", "Bea")]
        [TestCase("Andris", "Angela")]
        public void SendMessage_ThroughMatch_Successfully(string senderName, string receiverName)
        {
            var sender = Repo.GetAll().Single(x => x.Nickname == senderName);
            var receiver = Repo.GetAll().Single(x => x.Nickname == receiverName);
            MATCHES match = new MATCHES()
            {
                MatchDate = DateTime.Now,
                USER = sender,
                User2_ID = receiver.UserID,
                USERMESSAGES = new List<USERMESSAGES>()
            };
            Repo.SendMessage(sender, match, "TestMessage to: " + receiver.Nickname, string.Empty, ContentType.String);

            //Message content is right?
            Assert.That(sender.USERMESSAGES.FirstOrDefault().Content, Is.EqualTo("TestMessage to: " + receiver.Nickname));
            //other user of the match is the same user who is recieving the message?
            //Assert.That(sender.USERMESSAGES.FirstOrDefault().Match.User2_ID, Is.EqualTo(otherUser.UserID));
        }
        */
        /*
        [TestCase("Andris", "Angela")]
        public void CountOfLikes_Sender_IsCorrect(string senderName, string receiverName)
        {
            var sender = Repo.GetAll().Single(x => x.Nickname == senderName);
            var receiver = Repo.GetAll().Single(x => x.Nickname == receiverName);

            Repo.AddLike(sender, receiver, true);

            Assert.That(sender.Likes.Count(), Is.EqualTo(1));
        }
        [TestCase("Andris", "Angela")]
        [TestCase("Erika", "Ferenc")]
        public void BothLike_EndUp_WithMatch(string senderName, string receiverName)
        {
            var sender = Repo.GetAll().Single(x => x.Nickname == senderName);
            var receiver = Repo.GetAll().Single(x => x.Nickname == receiverName);

            Repo.AddLike(sender, receiver, true);
            Repo.AddLike(receiver, sender, true);

            var Matchername = Repo.FindUserByID(sender.MATCHES.First().User2_ID).Nickname;

            Assert.That(sender.Likes.Count(), Is.EqualTo(1));
            Assert.That(receiver.Likes.Count(), Is.EqualTo(1));
            Assert.That(sender.MATCHES.Count(), Is.EqualTo(1));
            Assert.That(receiver.MATCHES.Count(), Is.EqualTo(1));
            Assert.That(Matchername, Is.EqualTo(receiverName));
        }

        [TestCase("Andris", "Dia", "Erika")]
        public void ReturnMatches_Count_IsCorrect(string userMale, string userFemale1, string userFemale2)
        {
            var male = Repo.GetAll().Single(x => x.Nickname == userMale);
            var female1 = Repo.GetAll().Single(x => x.Nickname == userFemale1);
            var female2 = Repo.GetAll().Single(x => x.Nickname == userFemale2);

            Repo.AddLike(male, female1, true);
            Repo.AddLike(male, female2, true);
            Repo.AddLike(female1, male, true);
            Repo.AddLike(female2, male, true);

            Assert.That(Repo.LoadMatches(male).Count(), Is.EqualTo(2));
            Assert.That(Repo.LoadMatches(female1).Count(), Is.EqualTo(1));
            Assert.That(Repo.LoadMatches(female2).Count(), Is.EqualTo(1));
        }

        [TestCase("Andris", "Dia", "Erika")]
        public void ReturnMatches_Returns_CorrectUser(string userMale, string userFemale1, string userFemale2)
        {
            var male = Repo.GetAll().Single(x => x.Nickname == userMale);
            var female1 = Repo.GetAll().Single(x => x.Nickname == userFemale1);
            var female2 = Repo.GetAll().Single(x => x.Nickname == userFemale2);

            Repo.AddLike(male, female1, true);
            Repo.AddLike(male, female2, true);
            Repo.AddLike(female1, male, true);
            Repo.AddLike(female2, male, true);

            Assert.That(Repo.LoadMatches(male).First().Nickname, Is.EqualTo(userFemale1));
        }
        
        [TestCase("Andris", "Dia", "uzenet")]
        public void MatchMessages_ReturnMessages_CountIsCorrect(string maleName, string femaleName, string message)
        {
            var male = Repo.GetAll().Single(x => x.Nickname == maleName);
            var female = Repo.GetAll().Single(x => x.Nickname == femaleName);

            Repo.AddLike(male, female, true);
            Repo.AddLike(female, male, true);

            Repo.SendMessage(male, male.MATCHES.First(), message + "1 to " + female.Nickname + " from " + male.Nickname, string.Empty, ContentType.String);
            Repo.SendMessage(female, female.MATCHES.First(), message + "2 to " + male.Nickname + " from " + female.Nickname, string.Empty, ContentType.String);
            Repo.SendMessage(male, male.MATCHES.First(), message + "3 to " + female.Nickname + " from " + male.Nickname, string.Empty, ContentType.String);
            Repo.SendMessage(male, male.MATCHES.First(), message + "4 to " + female.Nickname + " from " + male.Nickname, string.Empty, ContentType.String);

            var messages = Repo.LoadMatchMessages(male, female);
            Assert.That(messages.Count(), Is.EqualTo(4));
        }

        //[TestCase("Andris","Angela")]
        //public void SendMessage_WithoutMatch_CountIsCorrect(string senderName, string receiverName)
        //{
        //    var sender = Repo.GetAll().Single(x => x.Nickname == senderName);
        //    var receiver = Repo.GetAll().Single(x => x.Nickname == receiverName);
        //    Repo.SendMessage(sender,null,)

        //}
        */
    }
}
