﻿namespace AppMutualInterfaces
{
    using System;
    public interface IMessage
    {
        int MessageID { get; }
        int? MatchID { get; }
        int SenderID { get; }

        int? DateID { get; }

        DateTime MessageDate { get;}
        string Content { get;  }
        string ContentPhoto { get;}
        string ContentType { get; }

        
        bool Read { get; set; }
    }
}
