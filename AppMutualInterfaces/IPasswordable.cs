﻿namespace AppMutualInterfaces
{
    /// <summary>
    /// Represents an object that has a password.
    /// </summary>
    public interface IPasswordable
    {
        /// <summary>
        /// The password of the object.
        /// </summary>
        string PassW { get; set; }
    }
}
