﻿namespace AppMutualInterfaces
{
    /// <summary>
    /// Represents an object by its DateID.
    /// </summary>
    public interface IDate
    {
        /// <summary>
        /// The identifier of the object.
        /// </summary>
        int DateID { get; set; }
    }
}
