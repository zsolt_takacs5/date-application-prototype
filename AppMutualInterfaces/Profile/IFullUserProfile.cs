﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppMutualInterfaces
{
    public interface IFullUserProfile : IUserIdentifiable, IProfile
    {
        string Email { get; }
         bool Interested { get; }
         int MinAge { get; }
         int MaxAge { get; }
         string Photo1 { get; }
         string Photo2 { get; }
         string Photo3 { get; }
         string Photo4 { get; }
         string Photo5 { get; }
         DateTime RegisteredDate { get; }
         bool Verified { get; }
         int? SubscriptionLevel { get; }
    }
}
