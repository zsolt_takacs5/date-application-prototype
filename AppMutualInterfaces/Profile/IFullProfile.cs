﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppMutualInterfaces
{
    /// <summary>
    /// Represents a fully viewable profile object.
    /// </summary>
    public interface IFullProfile : IUserIdentifiable, IProfile
    {
        /// <summary>
        /// The nickname of the object.
        /// </summary>
        string Nickname { get; set; }
        /// <summary>
        /// The age of the object.
        /// </summary>
        int Age { get; set; }
        /// <summary>
        /// The gender of the object.
        /// </summary>
        bool Male { get; set; }
        /// <summary>
        /// Holds the path to the main photo.
        /// </summary>
        string Photo1 { get; set; }
        /// <summary>
        /// Holds the path to the second photo.
        /// </summary>
        string Photo2 { get; set; }
        /// <summary>
        /// Holds the path to the third photo.
        /// </summary>
        string Photo3 { get; set; }
        /// <summary>
        /// Holds the path to the fourth photo.
        /// </summary>
        string Photo4 { get; set; }
        /// <summary>
        /// Holds the path to the fifth photo.
        /// </summary>
        string Photo5 { get; set; }
    }
}
