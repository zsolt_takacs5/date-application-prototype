﻿namespace AppMutualInterfaces
{
    using System.Collections.Generic;

    public enum ProfileType
    {
        RestrictedProfile,
        FullProfile,
        FullUserProfile
    }
    public interface IProfile
    {
        Dictionary<string,bool> Abilities { get; set; }
        //object PlusData { get; }
    }
}
