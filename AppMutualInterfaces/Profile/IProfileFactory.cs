﻿namespace AppMutualInterfaces
{
    public interface IProfileFactory
    {
        IProfile GetProfile(IUser userData, ProfileType profileType);
    }
}
