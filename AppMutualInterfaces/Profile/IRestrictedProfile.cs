﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppMutualInterfaces
{
    /// <summary>
    /// Represents a restricted viewable profile object.
    /// </summary>
    public interface IRestrictedProfile : IUserIdentifiable, IProfile
    {
        /// <summary>
        /// The nickname of the object.
        /// </summary>
        string Nickname { get; set; }
        /// <summary>
        /// The age of the object.
        /// </summary>
        int Age { get; set; }
        /// <summary>
        /// The gender of the object.
        /// </summary>
        bool Male { get; set; }
        /// <summary>
        /// The verification status of the object.
        /// </summary>
        bool Verified { get; set; }
    }
}
