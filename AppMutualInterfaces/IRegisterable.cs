﻿namespace AppMutualInterfaces
{
    public interface IRegisterable : ICredentialable
    {
        bool Gender { get; set; }
        bool Interested { get; set; }
        int MinAge { get; set; }
        int MaxAge { get; set; }
        int Age { get; set; }
        string Name { get; set; }
    }
}
