﻿namespace AppMutualInterfaces
{
    /// <summary>
    /// This interface derives from IEmailIdentifiable and IPasswordable
    /// </summary>
    public interface ICredentialable : IEmailIdentifiable, IPasswordable
    {
    }
}
