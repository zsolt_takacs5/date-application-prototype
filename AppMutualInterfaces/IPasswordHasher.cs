﻿namespace AppMutualInterfaces
{
    /// <summary>
    /// Represents a set of functions for generating and verifying passwords.   
    /// </summary>
    public interface IPasswordHasher
    {
        /// <summary>
        /// Hashes a password.
        /// </summary>
        /// <param name="password">The plain text of the password.</param>
        /// <returns>The hashed password.</returns>
        string HashPassword(string password);

        /// <summary>
        /// Compares a password with it's hashed version.
        /// </summary>
        /// <param name="hashedPassword">The hash of the original password.</param>
        /// <param name="providedPassword">The plain text of a desired password to.</param>
        /// <returns>The result of the comparison.</returns>
        bool VerifyHashedPassword(string hashedPassword, string providedPassword);
    }
}
