﻿namespace AppMutualInterfaces
{
    /// <summary>
    /// Represents an object by it's ID.
    /// </summary>
    public interface IUserIdentifiable
    {
        /// <summary>
        /// The UserID of the object.
        /// </summary>
        int UserID { get; set; }
    }
}
