﻿namespace AppMutualInterfaces
{
    using System;
    public interface IMatch
    {
        int MatchID { get;  }
        int User1_ID { get;}
        int User2_ID { get;}
        DateTime MatchDate { get;}
    }
}
