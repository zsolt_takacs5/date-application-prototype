﻿namespace AppMutualInterfaces
{
    /// <summary>
    /// Represents an object by its e-mail adress.
    /// </summary>
    public interface IEmailIdentifiable
    {
        /// <summary>
        /// The e-mail address of the object.
        /// </summary>
        string EMail { get; set; }
    }
}
