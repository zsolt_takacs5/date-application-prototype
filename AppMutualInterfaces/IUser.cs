﻿namespace AppMutualInterfaces
{
    using System;

    /// <summary>
    /// Represents a user profile object.
    /// </summary>
    public interface IUser : ICredentialable
    {
        /// <summary>
        /// The ID of the user.
        /// </summary>
        int UserID { get; set; }

        /// <summary>
        /// The nickname of the user.
        /// </summary>
        string Nickname { get; set; }

        /// <summary>
        /// The age of the user.
        /// </summary>
        int Age { get; set; }

        /// <summary>
        /// The e-mail adress of the user.
        /// </summary>
        string EMail { get; set; }

        /// <summary>
        /// The password of the user.
        /// </summary>
        string PassW { get; set; }

        /// <summary>
        /// The gender of the user.
        /// </summary>
        bool Male { get; set; }

        /// <summary>
        /// Interest of the user.
        /// </summary>
        bool Interested { get; set; }

        /// <summary>
        /// User's preference of partner's minimum age.
        /// </summary>
        int MinAge { get; set; }

        /// <summary>
        /// User's preference of partner's maximum age.
        /// </summary>
        int MaxAge { get; set; }

        /// <summary>
        /// The path to the user's main photo.
        /// </summary>
        string Photo1 { get; set; }

        /// <summary>
        /// The path to the user's second photo.
        /// </summary>
        string Photo2 { get; set; }

        /// <summary>
        /// The path to the user's third photo.
        /// </summary>
        string Photo3 { get; set; }

        /// <summary>
        /// The path to the user's fourth photo.
        /// </summary>
         string Photo4 { get; set; }

        /// <summary>
        /// The path to the user's fifth photo.
        /// </summary>
         string Photo5 { get; set; }

        /// <summary>
        /// The path to the user's first live photo.
        /// </summary>
         string Live1 { get; set; }

        /// <summary>
        /// The path to the user's second live photo.
        /// </summary>
         string Live2 { get; set; }

        /// <summary>
        /// The path to the user's third live photo.
        /// </summary>
         string Live3 { get; set; }

        /// <summary>
        /// The verification status of the user.
        /// </summary>
         bool Verified { get; set; }

        /// <summary>
        /// Tells if the user wants to date or not.
        /// </summary>
         bool WantToDate { get; set; }

        /// <summary>
        /// The user's subscription level.
        /// </summary>
         int? SubscriptionLevel { get; set; }

        /// <summary>
        /// The date when the user subscribed.
        /// </summary>
         DateTime? SubscriptionDate { get; set; }

        /// <summary>
        /// The date when the user registered.
        /// </summary>
         DateTime RegisteredDate { get; set; }

        /// <summary>
        /// The ID of the user's current Date object.
        /// </summary>
         int? CurrentDate { get; set; }

    }
}
