﻿namespace ApplicationPrototype
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using AppMutualInterfaces;

    /// <summary>
    /// This represents a date between two users.
    /// </summary>
    [Table("DATINGS")]
    public partial class DATINGS : IDate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DATINGS()
        {
            USERMESSAGES = new HashSet<USERMESSAGES>();
            USERS = new HashSet<USERS>();
        }

        /// <summary>
        /// The primary identifier of the date.
        /// </summary>
        [Key]
        public int DateID { get; set; }

        /// <summary>
        /// The colection of messages that this date contains.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USERMESSAGES> USERMESSAGES { get; set; }

        /// <summary>
        /// The collection of users that participating in the date. Must only contains 2 users.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USERS> USERS { get; set; }
    }
}
