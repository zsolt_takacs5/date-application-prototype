﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPrototype;
using System.Data.Entity;

namespace ApplicationPrototype
{
    public interface IDateContext
    {
        DbSet<LIKES> LIKES { get; set; }
        /// <summary>
        /// Gets access to the MATCHES table.
        /// </summary>
        DbSet<MATCHES> MATCHES { get; set; }
        /// <summary>
        /// Gets access to the USERMESSAGES table.
        /// </summary>
        DbSet<USERMESSAGES> USERMESSAGES { get; set; }
        /// <summary>
        /// Gets access to the USERS table.
        /// </summary>
        DbSet<USERS> USERS { get; set; }
        /// <summary>
        /// Gets access to the DATINGS table.
        /// </summary>
        DbSet<DATINGS> DATINGS { get; set; }

        void OnModelCreating(DbModelBuilder modelBuilder);
        void SaveChanges();
    }
}
