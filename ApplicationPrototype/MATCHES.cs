namespace ApplicationPrototype
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using AppMutualInterfaces;

    public partial class MATCHES : IMatch
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MATCHES()
        {
            USERMESSAGES = new HashSet<USERMESSAGES>();
        }

        [Key]
        public int MatchID { get; set; }

        public int User1_ID { get; set; }

        public int User2_ID { get; set; }

        public DateTime MatchDate { get; set; }

        public virtual USERS USER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USERMESSAGES> USERMESSAGES { get; set; }
    }
}
