namespace ApplicationPrototype
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using AppMutualInterfaces;
    
    /// <summary>
    /// This class represents a record of the USERS table from the database. 
    /// </summary>
    public partial class USERS : IUser /*, IFullProfile, IRestrictedProfile*/
    {
        /// <summary>
        /// Creates a new USERS object.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public USERS()
        {
            Likes = new HashSet<LIKES>();
            LikedBy = new HashSet<LIKES>();
            MATCHES = new HashSet<MATCHES>();
            USERMESSAGES = new HashSet<USERMESSAGES>();
        }

        /// <summary>
        /// The primary identifier of the user.
        /// </summary>
        [Key]
        public int UserID { get; set; }

        /// <summary>
        /// The nickname of the user.
        /// </summary>
        [Required]
        [StringLength(25)]
        public string Nickname { get; set; }

        /// <summary>
        /// The age of the user.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// The e-mail address of the user.
        /// </summary>
        [Required]
        [StringLength(50)]
        public string EMail { get; set; }

        /// <summary>
        /// The password of the user.
        /// </summary>
        [Required]
        [StringLength(1000)]
        public string PassW { get; set; }

        /// <summary>
        /// Determines if the user is male or not. True is male, False is female.
        /// </summary>
        public bool Male { get; set; }

        /// <summary>
        /// This represents the user's interest. True is male, False is female.
        /// </summary>
        public bool Interested { get; set; }

        /// <summary>
        /// This is a search criteria for the user. User is looking for a partner above this age.
        /// </summary>
        public int MinAge { get; set; }

        /// <summary>
        /// This is a search criteria for the user. User is looking for a partner below this age.
        /// </summary>
        public int MaxAge { get; set; }

        /// <summary>
        /// The path on the server of the user's main photo.
        /// </summary>
        [StringLength(300)]
        public string Photo1 { get; set; }

        /// <summary>
        /// The path on the server of the user's second photo.
        /// </summary>
        [StringLength(300)]
        public string Photo2 { get; set; }

        /// <summary>
        /// The path on the server of the user's third photo.
        /// </summary>
        [StringLength(300)]
        public string Photo3 { get; set; }

        /// <summary>
        /// The path on the server of the user's fourth photo.
        /// </summary>
        [StringLength(300)]
        public string Photo4 { get; set; }

        /// <summary>
        /// The path on the server of the user's fifth photo.
        /// </summary>
        [StringLength(300)]
        public string Photo5 { get; set; }

        /// <summary>
        /// The path on the server of the user's first live photo for verification.
        /// </summary>
        [StringLength(300)]
        public string Live1 { get; set; }

        /// <summary>
        /// The path on the server of the user's second live photo for verification.
        /// </summary>
        [StringLength(300)]
        public string Live2 { get; set; }

        /// <summary>
        /// The path on the server of the user's third live photo for verification.
        /// </summary>
        [StringLength(300)]
        public string Live3 { get; set; }
        
        /// <summary>
        /// Tells if the user is verified or not.
        /// </summary>
        public bool Verified { get; set; }

        /// <summary>
        /// Tells if the user is in a want-to-date status. 
        /// </summary>
        public bool WantToDate { get; set; }

        /// <summary>
        /// The subscription level of the user. Nullable.
        /// </summary>
        public int? SubscriptionLevel { get; set; }

        /// <summary>
        /// The subscription date of the user. Nullable.
        /// </summary>
        public DateTime? SubscriptionDate { get; set; }

        /// <summary>
        /// The registration date of the user.
        /// </summary>
        public DateTime RegisteredDate { get; set; }

        /// <summary>
        /// The primary key of the current DATINGS object, that the user is in.
        /// </summary>
        /// <see cref="DATINGS"/>
        public int? CurrentDate { get; set; }

        /// <summary>
        /// Collection of likes that the user give.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LIKES> Likes { get; set; }

        /// <summary>
        /// Collection of likes that the user got.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LIKES> LikedBy { get; set; }

        /// <summary>
        /// Collection of matches that the user has.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MATCHES> MATCHES { get; set; }

        /// <summary>
        /// Collection of messages that the user ever sent.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USERMESSAGES> USERMESSAGES { get; set; }

        /// <summary>
        /// Collection of users that got access to the current user's album.
        /// </summary>
        virtual public ICollection<USERS> AlbumSharedWith { get; set; }

        /// <summary>
        /// Collection of users that provided access to their album for the current user. 
        /// </summary>
        virtual public ICollection<USERS> AlbumSharedBy { get; set; }

        /// <summary>
        /// The current date that the user is participating. 
        /// </summary>
        /// <see cref="DATINGS"/>
        public virtual DATINGS Date { get; set; }

        public override string ToString()
        {
            return this.UserID + ", Name: "
                + this.Nickname + ", Age: "
                + this.Age + ", Email: "
                + this.EMail + ", Gender: "
                + GenderToString(this.Male) + ", Interested in: "
                + GenderToString(this.Interested) + ", Likes: "
                + this.Likes.Count + ", Matches: "
                + this.MATCHES.Count;
        }
        private string GenderToString(bool gender)
        {
            return gender ? "Male" : "Female";
        }
    }

}
