namespace ApplicationPrototype
{
    using System;
    using System.Data.Entity;


    /// <summary>
    /// This class represents the database connection. Inherited from DbContext 
    /// </summary>
    public partial class AppModell : DbContext, IDisposable//, IDateContext
    {
        /// <summary>
        /// Connects to the database.
        /// </summary>
        public AppModell()
            : base("name=AppModellDesktop") //AppModell - laptop, AppModellDesktop - desktop server
        {
        }

        /// <summary>
        /// Gets access to the LIKES table.
        /// </summary>
        public virtual DbSet<LIKES> LIKES { get; set; }
        /// <summary>
        /// Gets access to the MATCHES table.
        /// </summary>
        public virtual DbSet<MATCHES> MATCHES { get; set; }
        /// <summary>
        /// Gets access to the USERMESSAGES table.
        /// </summary>
        public virtual DbSet<USERMESSAGES> USERMESSAGES { get; set; }
        /// <summary>
        /// Gets access to the USERS table.
        /// </summary>
        public virtual DbSet<USERS> USERS { get; set; }
        /// <summary>
        /// Gets access to the DATINGS table.
        /// </summary>
        public virtual DbSet<DATINGS> DATINGS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DATINGS>()
                .HasMany(e => e.USERS)
                .WithOptional(e => e.Date)
                .HasForeignKey(e => e.CurrentDate);

            modelBuilder.Entity<USERMESSAGES>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<USERMESSAGES>()
                .Property(e => e.ContentPhoto)
                .IsUnicode(false);

            modelBuilder.Entity<USERMESSAGES>()
                .Property(e => e.ContentType)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Nickname)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.EMail)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.PassW)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Photo1)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Photo2)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Photo3)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Photo4)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Photo5)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Live1)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Live2)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .Property(e => e.Live3)
                .IsUnicode(false);

            modelBuilder.Entity<USERS>()
                .HasMany(e => e.Likes)
                .WithRequired(e => e.JudgeUser)
                .HasForeignKey(e => e.Judge)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<USERS>()
                .HasMany(e => e.LikedBy)
                .WithRequired(e => e.JudgedUser)
                .HasForeignKey(e => e.JudgedPerson)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<USERS>()
                .HasMany(e => e.MATCHES)
                .WithRequired(e => e.USER)
                .HasForeignKey(e => e.User1_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<USERS>()
                .HasMany(e => e.USERMESSAGES)
                .WithRequired(e => e.USERS)
                .HasForeignKey(e => e.SenderID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<USERS>().HasMany(m => m.AlbumSharedWith).WithMany(m => m.AlbumSharedBy).Map(m =>
            {
                m.MapLeftKey("SharerUserID");
                m.MapRightKey("ReceiverUserID");
                m.ToTable("AlbumShare");
            });

        }
    }
}
