namespace ApplicationPrototype
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This represents a like between 2 users.
    /// </summary>
    /// <see cref="USERS"/>
    public partial class LIKES
    {
        /// <summary>
        /// One of the primary identifier of the like. Represents the user who gave the like.
        /// </summary>
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Judge { get; set; }

        /// <summary>
        /// One of the primary identifier of the like. Represents the user who got the like.
        /// </summary>
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JudgedPerson { get; set; }

        /// <summary>
        /// Represents if the like was a LIKE or a DISLIKE.
        /// </summary>
        public bool Liked { get; set; }

        /// <summary>
        /// The user who liked the other user.
        /// </summary>
        public virtual USERS JudgeUser { get; set; }

        /// <summary>
        /// The user who got liked by the other user.
        /// </summary>
        public virtual USERS JudgedUser { get; set; }
    }
}
