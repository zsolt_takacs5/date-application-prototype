namespace ApplicationPrototype.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class photoSectionUpdateTo300 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.USERS", "Photo1", c => c.String(maxLength: 300, unicode: false));
            AlterColumn("dbo.USERS", "Photo2", c => c.String(maxLength: 300, unicode: false));
            AlterColumn("dbo.USERS", "Photo3", c => c.String(maxLength: 300, unicode: false));
            AlterColumn("dbo.USERS", "Photo4", c => c.String(maxLength: 300, unicode: false));
            AlterColumn("dbo.USERS", "Photo5", c => c.String(maxLength: 300, unicode: false));
            AlterColumn("dbo.USERS", "Live1", c => c.String(maxLength: 300, unicode: false));
            AlterColumn("dbo.USERS", "Live2", c => c.String(maxLength: 300, unicode: false));
            AlterColumn("dbo.USERS", "Live3", c => c.String(maxLength: 300, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.USERS", "Live3", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.USERS", "Live2", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.USERS", "Live1", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.USERS", "Photo5", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.USERS", "Photo4", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.USERS", "Photo3", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.USERS", "Photo2", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.USERS", "Photo1", c => c.String(maxLength: 100, unicode: false));
        }
    }
}
