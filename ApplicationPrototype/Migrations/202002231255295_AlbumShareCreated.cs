namespace ApplicationPrototype.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlbumShareCreated : DbMigration
    {
        public override void Up()
        {
            /*
            CreateTable(
                "dbo.DATINGS",
                c => new
                    {
                        DateID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.DateID);
            
            CreateTable(
                "dbo.USERMESSAGES",
                c => new
                    {
                        MessageID = c.Int(nullable: false, identity: true),
                        MatchID = c.Int(),
                        SenderID = c.Int(nullable: false),
                        MessageDate = c.DateTime(nullable: false),
                        Content = c.String(maxLength: 200, unicode: false),
                        ContentPhoto = c.String(maxLength: 200, unicode: false),
                        DateID = c.Int(),
                        ContentType = c.String(nullable: false, maxLength: 10, unicode: false),
                    })
                .PrimaryKey(t => t.MessageID)
                .ForeignKey("dbo.DATINGS", t => t.DateID)
                .ForeignKey("dbo.USERS", t => t.SenderID)
                .ForeignKey("dbo.MATCHES", t => t.MatchID)
                .Index(t => t.MatchID)
                .Index(t => t.SenderID)
                .Index(t => t.DateID);
            
            CreateTable(
                "dbo.MATCHES",
                c => new
                    {
                        MatchID = c.Int(nullable: false, identity: true),
                        User1_ID = c.Int(nullable: false),
                        User2_ID = c.Int(nullable: false),
                        MatchDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MatchID)
                .ForeignKey("dbo.USERS", t => t.User1_ID)
                .Index(t => t.User1_ID);
            
            CreateTable(
                "dbo.USERS",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        Nickname = c.String(nullable: false, maxLength: 25, unicode: false),
                        Age = c.Int(nullable: false),
                        EMail = c.String(nullable: false, maxLength: 50, unicode: false),
                        PassW = c.String(nullable: false, maxLength: 1000, unicode: false),
                        Male = c.Boolean(nullable: false),
                        Interested = c.Boolean(nullable: false),
                        MinAge = c.Int(nullable: false),
                        MaxAge = c.Int(nullable: false),
                        Photo1 = c.String(maxLength: 100, unicode: false),
                        Photo2 = c.String(maxLength: 100, unicode: false),
                        Photo3 = c.String(maxLength: 100, unicode: false),
                        Photo4 = c.String(maxLength: 100, unicode: false),
                        Photo5 = c.String(maxLength: 100, unicode: false),
                        Live1 = c.String(maxLength: 100, unicode: false),
                        Live2 = c.String(maxLength: 100, unicode: false),
                        Live3 = c.String(maxLength: 100, unicode: false),
                        Verified = c.Boolean(nullable: false),
                        WantToDate = c.Boolean(nullable: false),
                        SubscriptionLevel = c.Int(),
                        SubscriptionDate = c.DateTime(),
                        RegisteredDate = c.DateTime(nullable: false),
                        CurrentDate = c.Int(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.DATINGS", t => t.CurrentDate)
                .Index(t => t.CurrentDate);
            
            CreateTable(
                "dbo.LIKES",
                c => new
                    {
                        Judge = c.Int(nullable: false),
                        JudgedPerson = c.Int(nullable: false),
                        Liked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.Judge, t.JudgedPerson })
                .ForeignKey("dbo.USERS", t => t.JudgedPerson)
                .ForeignKey("dbo.USERS", t => t.Judge)
                .Index(t => t.Judge)
                .Index(t => t.JudgedPerson);
            */
            CreateTable(
                "dbo.AlbumShare",
                c => new
                    {
                        SharerUserID = c.Int(nullable: false),
                        ReceiverUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SharerUserID, t.ReceiverUserID })
                .ForeignKey("dbo.USERS", t => t.SharerUserID)
                .ForeignKey("dbo.USERS", t => t.ReceiverUserID)
                .Index(t => t.SharerUserID)
                .Index(t => t.ReceiverUserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.USERS", "CurrentDate", "dbo.DATINGS");
            DropForeignKey("dbo.USERMESSAGES", "MatchID", "dbo.MATCHES");
            DropForeignKey("dbo.USERMESSAGES", "SenderID", "dbo.USERS");
            DropForeignKey("dbo.MATCHES", "User1_ID", "dbo.USERS");
            DropForeignKey("dbo.LIKES", "Judge", "dbo.USERS");
            DropForeignKey("dbo.LIKES", "JudgedPerson", "dbo.USERS");
            DropForeignKey("dbo.AlbumShare", "ReceiverUserID", "dbo.USERS");
            DropForeignKey("dbo.AlbumShare", "SharerUserID", "dbo.USERS");
            DropForeignKey("dbo.USERMESSAGES", "DateID", "dbo.DATINGS");
            DropIndex("dbo.AlbumShare", new[] { "ReceiverUserID" });
            DropIndex("dbo.AlbumShare", new[] { "SharerUserID" });
            DropIndex("dbo.LIKES", new[] { "JudgedPerson" });
            DropIndex("dbo.LIKES", new[] { "Judge" });
            DropIndex("dbo.USERS", new[] { "CurrentDate" });
            DropIndex("dbo.MATCHES", new[] { "User1_ID" });
            DropIndex("dbo.USERMESSAGES", new[] { "DateID" });
            DropIndex("dbo.USERMESSAGES", new[] { "SenderID" });
            DropIndex("dbo.USERMESSAGES", new[] { "MatchID" });
            DropTable("dbo.AlbumShare");
            DropTable("dbo.LIKES");
            DropTable("dbo.USERS");
            DropTable("dbo.MATCHES");
            DropTable("dbo.USERMESSAGES");
            DropTable("dbo.DATINGS");
        }
    }
}
