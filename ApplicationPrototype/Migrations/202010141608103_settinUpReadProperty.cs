namespace ApplicationPrototype.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settinUpReadProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.USERMESSAGES", "Read", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.USERMESSAGES", "Read");
        }
    }
}
