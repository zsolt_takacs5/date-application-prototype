namespace ApplicationPrototype
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using AppMutualInterfaces;

    /// <summary>
    /// This represents messages between users, in dates and matches
    /// </summary>
    /// <see cref="DATINGS"/>
    /// <seealso cref="MATCHES"/>
    /// <seealso cref="ApplicationPrototype.USERS"/>
    public partial class USERMESSAGES : IMessage
    {
        /// <summary>
        /// The primary identifier of the message.
        /// </summary>
        [Key]
        public int MessageID { get; set; }

        /// <summary>
        /// The match identifier that this message is part of. Nullable.
        /// </summary>
        public int? MatchID { get; set; }

        /// <summary>
        /// The primary identifier of the user that sent this message.
        /// </summary>
        public int SenderID { get; set; }

        /// <summary>
        /// The date of the message is created.
        /// </summary>
        public DateTime MessageDate { get; set; }

        /// <summary>
        /// The content of the message.
        /// </summary>
        [StringLength(200)]
        public string Content { get; set; }

        /// <summary>
        /// The path to a photo if the message contains a photo.
        /// </summary>
        [StringLength(200)]
        public string ContentPhoto { get; set; }

        /// <summary>
        /// The primary identifier of the date that this message is part of.
        /// </summary>
        public int? DateID { get; set; }

        /// <summary>
        /// The content type of the message. Photo, String, Both.
        /// </summary>
        [Required]
        [StringLength(10)]
        public string ContentType { get; set; }
        
        /// <summary>
        /// The match that this message is part of.
        /// </summary>
        public virtual MATCHES Match { get; set; }

        /// <summary>
        /// The sender of the message.
        /// </summary>
        public virtual USERS USERS { get; set; }

        /// <summary>
        /// The date that this message is part of.
        /// </summary>
        public virtual DATINGS Date { get; set; }
        
        public bool Read { get; set; }

        
    }
}
