﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPrototype;

namespace ApplicationPrototype
{
    public interface IMatch
    {
        int MatchID { get; set; }
        int User1_ID { get; set; }
        int User2_ID { get; set; }
        DateTime MatchDate { get; set; }
        USERS USER { get; set; }
        ICollection<USERMESSAGES> USERMESSAGES { get; set; }
    }
}
