﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPrototype;

namespace ApplicationPrototype
{
    public interface IUser
    {
        int UserID { get; set; }
        string Nickname { get; set; }
        int Age { get; set; }
        string EMail { get; set; }
        string PassW { get; set; }
        bool Male { get; set; }
        bool Interested { get; set; }
        int MinAge { get; set; }
        int MaxAge { get; set; }
        string Photo1 { get; set; }
        string Photo2 { get; set; }
        string Photo3 { get; set; }
        string Photo4 { get; set; }
        string Photo5 { get; set; }
        string Live1 { get; set; }
        string Live2 { get; set; }
        string Live3 { get; set; }
        bool Verified { get; set; }
        bool WantToDate { get; set; }
        int? SubscriptionLevel { get; set; }
        DateTime? SubscriptionDate { get; set; }
        DateTime RegisteredDate { get; set; }
        ICollection<LIKES> Likes { get; set; }
        ICollection<LIKES> LikedBy { get; set; }
        ICollection<MATCHES> MATCHES { get; set; }
        ICollection<USERMESSAGES> USERMESSAGES { get; set; }
    }
}
