﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPrototype;

namespace ApplicationPrototype
{
    public interface ILike
    {
        int Judge { get; set; }
        int JudgedPerson { get; set; }
        bool Liked { get; set; }
        USERS JudgeUser { get; set; }
        USERS JudgedUser { get; set; }
    }
}
