﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPrototype;

namespace ApplicationPrototype
{
    interface IMessage
    {
        int MessageID { get; set; }

        int? MatchID { get; set; }

        int SenderID { get; set; }

        DateTime MessageDate { get; set; }

        string Content { get; set; }

        string ContentPhoto { get; set; }

        string ContentType { get; set; }

        MATCHES Match { get; set; }

        USERS USERS { get; set; }
    }
}
