﻿namespace Repository
{
    using System.Linq;
    using ApplicationPrototype;

    public class DateRepository : IDateRepository
    {
        private AppModell model;

        public DateRepository(AppModell model)
        {
            this.model = model;
        }
        public void Save(AppModell model)
        {
            model.SaveChanges();
        }
        public void Create(DATINGS date)
        {
            //AppModell model = new AppModell();
            model.DATINGS.Add(date);
            Save(model);
        }
        public IQueryable<DATINGS> GetAll()
        {
            return model.DATINGS.AsQueryable();
        }
        public void Update(DATINGS date)
        {
            
            Save(model);
        }
        public void Delete(DATINGS date)
        {
            
            model.DATINGS.Remove(date);
            Save(model);
        }

        /// <summary>
        /// Search in the model for a specific DATE that has the ID. Returns it if it has been found, otherwise return null.
        /// </summary>
        /// <param name="dateID">The ID of the desired DATE object.</param>
        /// <returns>The DATE object that's ID is the same as the provided ID. Or null object if there is no DATE with that ID.</returns>
        public DATINGS FindByID(int dateID)
        {
            //AppModell model = new AppModell();
            var dbDate = model.DATINGS.Where(x => x.DateID == dateID).FirstOrDefault();
            return dbDate;
        }
    }
}
