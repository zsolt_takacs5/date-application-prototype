﻿namespace Repository
{
    using System.Linq;
    using ApplicationPrototype;
    using AppMutualInterfaces;
    using Repository.Exceptions;

    public class MatchRepository : IMatchRepository //FindBy should find it by 2 user's ID
    {
        private AppModell model;

        public MatchRepository(AppModell model)
        {
            this.model = model;
        }
        public void Update(MATCHES entity)
        {
            Save(model);
        }
        public void Create(MATCHES entity)
        {
            model.MATCHES.Add(entity);
            Save(model);
        }
        public IQueryable<MATCHES> GetAll()
        {
            return model.MATCHES.AsQueryable();
        }
        public void Delete(MATCHES entity)
        {
            model.MATCHES.Remove(entity);
            Save(model);
        }
        public MATCHES FindById(int ID)
        {
            return this.GetAll().Where(x=>x.MatchID == ID).FirstOrDefault();
        }
        public MATCHES FindByUsers(IUser userA, IUser userB)
        {
            var matches = this.GetAll();
            return matches.Where(x => x.User1_ID == userA.UserID && x.User2_ID == userB.UserID).FirstOrDefault();
        }
        public IMatch FindIMatchById(int ID)
        {
            var match = this.GetAll().Where(x => x.MatchID == ID).FirstOrDefault();
            return match;
        }
        public MATCHES FindByID(int ID)
        {
            var match = this.GetAll().Where(x => x.MatchID == ID).FirstOrDefault();
            if (match == null)
            {
                throw new NotFoundException<MATCHES>("ID",null);
            }
            return match;
        }
        public void Save(AppModell model)
        {
            model.SaveChanges();
        }
    }
}
