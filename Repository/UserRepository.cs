﻿namespace Repository
{
    using System.Linq;
    using System.Text;
    using ApplicationPrototype;
    using AppMutualInterfaces;
    using System.Security.Cryptography;
    using Repository.Exceptions;
    using System;

    public class UserRepository : IUserRepository
    {
        private AppModell model;

        public UserRepository(AppModell modell)
        {
            this.model = modell;
        }

        /// <summary>
        /// Creates a new USER entity with properties of the parameter.
        /// </summary>
        /// <param name="entity">The entity that needs to be added.</param>
        /// <exception cref="MultipleEmailException">Thrown when the provided entity's email address has already in the database.</exception>
        public virtual void Create(USERS entity)
        {
            //if (this.FindUserByEmail(entity.EMail) == null)
            //{
            //    throw new MultipleEmailException("E-mail is already being used. Please provide another e-mail address.");
            //}
            //else
            //{
                //AppModell model = new AppModell();
                model.USERS.Add(entity);
                Save(model);
            //}
            /*
            AppModell model = new AppModell();
            model.USERS.Add(entity);
            Save(model);
            */
        }

        /// <summary>
        /// Updates an entity from the database with the properties of the provided entity.
        /// </summary>
        /// <param name="entity">The entity that needs to be updated.</param>
        public virtual void Update(USERS entity)
        {
            model.Entry<USERS>(entity).State = System.Data.Entity.EntityState.Modified;
            Save(model);
        }

        /// <summary>
        /// Deletes an entity from the database.
        /// </summary>
        /// <param name="entity">The entity that needs to be deleted.</param>
        public virtual void Delete(USERS entity)
        {
            //AppModell model = new AppModell();
            model.USERS.Remove(entity);
            Save(model);
        }

        /// <summary>
        /// Saves the database.
        /// </summary>
        /// <param name="model">The database model that needs to be saved. This type must be derived from DBContext abstract class.</param>
        public void Save(AppModell model)
        {
            model.SaveChanges();
        }

        /// <summary>
        /// Returns all the Users from the database.
        /// </summary>
        /// <returns>All the user entity from the database.</returns>
        public virtual IQueryable<USERS> GetAll()
        {
            //AppModell model = new AppModell();
            return model.USERS.AsQueryable();
        }

        public virtual IQueryable<USERS> GetAll(AppModell model)
        {
            return model.USERS.AsQueryable();
        }
        
        /// <summary>
        /// Finds a USER by the given ID. If User found, then it returns the User entity, otherwise returns null object.
        /// </summary>
        /// <param name="userID">The ID of a user.</param>
        /// <returns>User entity.</returns>
        public USERS FindByID(int userID)
        {
            return this.GetAll().Where(x => x.UserID == userID).FirstOrDefault();
        }

        public bool IsValidEmail(string email)
        {
            //SOURCE: https://stackoverflow.com/questions/1365407/c-sharp-code-to-validate-email-address
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }

        }

        public USERS FindUserByEmail(string email)
        {
            return this.GetAll().Where(x => x.EMail == email).FirstOrDefault();
        }

        /// <summary>
        /// Finds user by given Credentials (EMail and Password). Returns the user object if found; otherwise exception throw.
        /// </summary>
        /// <param name="loginUser">The user that needs to be found, represented by email and password.</param>
        /// <returns>The user from the database model.</returns>
        /// <exception cref="WrongCredentialsException">Thrown when there is no user with the corresponding credentials.</exception>
        public USERS FindUserByLoginCredentials(ICredentialable loginUser)
        {
            var user = this.FindUserByEmail(loginUser.EMail);
            if (user == null)
            {
                throw new WrongCredentialsException("The granted Email or Password was wrong.");
            }
            /* If user is somebody:
                1. Get the PW from the DBUser
                2. Hashing the loginUser PW
                3. Compare the hashes
                4. IF Equal : return user
                4. IF not Equal: return new WrongCredentialsException
            */
            var userPW = user.PassW;
            //NEED HASHING to loginUser.PW
            string hashedPass = HashPassword(loginUser.PassW);
            if (VerifyHashedPassword(user.PassW, loginUser.PassW))
            {
                return user;
            }
            else
	        {
                throw new WrongCredentialsException("The granted EMail or Password was wrong.");
            }
        }

        //Blockind and Disliking
        
        public void BlockUser(IUser user, IUser blockedUser)
        {
            //block logic here..
        }
        public void DislikeUser(IUser user, IUser dislikedUser)
        {
            //dislike logic here
            //AddLike(user, dislikedUser, false);
        }
        public void ReportUser(IUser user, string reportDescription)
        {
            //report logic here...
        }

        /// <summary>
        /// Hashes a given string.
        /// </summary>
        /// <param name="password">The password that needs to be hashed.</param>
        /// <returns>The hash of the given password.</returns>
        public string HashPassword(string password)
        {
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();
            string hash = BitConverter.ToString(provider.ComputeHash(passwordBytes)).Replace("-", "").ToLower();


            return hash;
        }

        /// <summary>
        /// Compares the given plain-text password with a hashed version.
        /// </summary>
        /// <param name="hashedPassword">The hash of a password, must be retrieved from the database.</param>
        /// <param name="providedPassword">The plain-text password that needs to be compared to the hash.</param>
        /// <returns>True if the provided password is the same as the hash.</returns>
        public bool VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            string providedHash = HashPassword(providedPassword);
            return providedHash == hashedPassword;
        }
        
    }
}
