﻿using System;

namespace Repository.Exceptions
{
    public class MultipleEmailException : Exception
    {
        public MultipleEmailException(string message) : base(message)
        {
        }
    }
}
