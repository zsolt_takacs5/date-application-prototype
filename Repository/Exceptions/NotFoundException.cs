﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Exceptions
{
    public class NotFoundException<T> : Exception
    {
        public NotFoundException(string credential, Exception innerException) : base($"{typeof(T).ToString()} not found with the given {typeof(T).ToString()} {credential}.", innerException)
        {
        }
    }
}
