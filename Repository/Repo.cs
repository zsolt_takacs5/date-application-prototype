﻿namespace Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using ApplicationPrototype;
    using AppMutualInterfaces;

    /// <summary>
    /// Represents a generic Repository structure with a set of CRUD functions to work on a database.
    /// </summary>
    /// <typeparam name="T">The type of the repository. This has to be a type generated from the datbase.</typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Returns all the database records of a given table(/type) as a queryable data structure.
        /// </summary>
        /// <returns>All the records of a database table.</returns>
        IQueryable<T> GetAll();
        
        /// <summary>
        /// Inserts an object into the database.
        /// </summary>
        /// <param name="entity">The object to insert.</param>
        void Create(T entity);

        /// <summary>
        /// Updates a record in the database, based on the given object.
        /// </summary>
        /// <param name="entity">The object with all the updated parameters.</param>
        void Update(T entity);

        /// <summary>
        /// Deletes a record from the database, based on the given object.
        /// </summary>
        /// <param name="entity">An object to be deleted.</param>
        void Delete(T entity);

        /// <summary>
        /// Updates the database with the generated database model.
        /// </summary>
        /// <param name="model">The generated database model.</param>
        void Save(AppModell model);
        
        /// <summary>
        /// Finds an object in the generated database model by it's ID.
        /// </summary>
        /// <param name="TEntityID">The ID of the object that needs to be found.</param>
        /// <returns>The desired object.</returns>
        T FindByID(int TEntityID);

    }
    /// <summary>
    /// Defines a set of types for a content.
    /// </summary>
    public enum ContentType
    {
        Photo,
        String,
        Both
    }
    /// <summary>
    /// Defines CRUD and additional functionality to work with 
    /// <see cref="USERS"/> from the database model.
    /// This interface implements the generic <see cref="IRepository{USERS}"/> interface.
    /// </summary>
    public interface IUserRepository : IRepository<USERS>, IPasswordHasher
    {
        /// <summary>
        /// Finds a <c>USERS</c> entity based a given e-mail address.
        /// </summary>
        /// <param name="email">The e-mail address of the desired entity.</param>
        /// <returns>The entity with the same e-mail address as the <paramref name="email"/> parameter. </returns>
        USERS FindUserByEmail(string email);

        /// <summary>
        /// Finds a <c>USERS</c> entity based on the given credentials.
        /// </summary>
        /// <param name="user">The credentials of the user.</param>
        /// <seealso cref="ICredentialable"/>
        /// <returns>The entity with the same credentials as the <paramref name="user"/> parameter.</returns>
        USERS FindUserByLoginCredentials(ICredentialable user);

        bool IsValidEmail(string email);
        
        
        void BlockUser(IUser currentUser, IUser blockUser);
        void DislikeUser(IUser currentUser, IUser dislikedUser);
        void ReportUser(IUser user, string reportDescription);
    }
    /// <summary>
    /// Defines CRUD and additional functionality to work with 
    /// <see cref="MATCHES"/> from the database model.
    /// This interface implements the generic <see cref="IRepository{MATCHES}"/> interface.
    /// </summary>
    public interface IMatchRepository : IRepository<MATCHES>
    {
        MATCHES FindById(int matchID);

        /// <summary>
        /// Finds a <c>MATCHES</c> entity based on the given
        /// <see cref="IUser"/> entities.
        /// </summary>
        /// <param name="userMain">The main user of the match.</param>
        /// <param name="userSecondary">The secondary user of the match.</param>
        /// <returns></returns>
        MATCHES FindByUsers(IUser userMain, IUser userSecondary);
    }
    /// <summary>
    /// Defines CRUD and additional functionality to wotk with
    /// <see cref="USERMESSAGES"/> from the database model.
    /// This interface implements the generic <see cref="IRepository{USERMESSAGES}"/> interface.
    /// </summary>
    public interface IMessageRepository : IRepository<USERMESSAGES>
    {
        ICollection<USERMESSAGES> FindByUserSent(IUser user);

        /// <summary>
        /// Returns the messages in a match.
        /// </summary>
        /// <param name="match">The match which messages would like to be retrieved.</param>
        /// <returns>Collection of messages.</returns>
        ICollection<USERMESSAGES> FindByMatch(IMatch match);
    }
    /// <summary>
    /// Defines CRUD and additional functionality to wotk with
    /// <see cref="LIKES"/> from the database model.
    /// This interface implements the generic <see cref="IRepository{LIKES}"/> interface.
    /// </summary>
    public interface ILikeRepository : IRepository<LIKES>
    {
        /// <summary>
        /// Returns all likes that the user give.
        /// </summary>
        /// <param name="user">The user that likes needs to be retrieved.</param>
        /// <returns>The collection of likes the user give.</returns>
        ICollection<LIKES> FindAllLikesUserGive(IUser user);

        /// <summary>
        /// Return all likes that a user got.
        /// </summary>
        /// <param name="user">The user that likes needs to be retrieved.</param>
        /// <returns>The collection of likes that the user got.</returns>
        ICollection<LIKES> FindAllLikesUserGot(IUser user);
        /// <summary>
        /// Return all the likes that a user got & give.
        /// </summary>
        /// <param name="user">The user that likes needs to be retrieved.</param>
        /// <returns>The collection of all the likes that the user got and give.</returns>
        ICollection<LIKES> FindAllLikesAssignedToUser(IUser user);
        /// <summary>
        /// Finds a like by the judge and judged.
        /// </summary>
        /// <param name="judge">The user who judged the other user.</param>
        /// <param name="judged">The user who was judged by the other user.</param>
        /// <returns>A like entity.</returns>
        LIKES FindByUsers(IUser judge, IUser judged);
    }
    /// <summary>
    /// The date Repository.
    /// </summary>
    public interface IDateRepository : IRepository<DATINGS>
    {
        //DATINGS FindDateByID(int dateID);
    }
}
