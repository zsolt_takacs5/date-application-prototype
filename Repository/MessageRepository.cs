﻿namespace Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using ApplicationPrototype;
    using AppMutualInterfaces;

    public class MessageRepository : IMessageRepository
    {
        private AppModell model;

        public MessageRepository(AppModell model)
        {
            this.model = model;
        }
        //CRUD methods
        public virtual void Create(USERMESSAGES entity)
        {
            model.USERMESSAGES.Add(entity);
            Save(model);
        }
        public virtual void Update(USERMESSAGES entity)
        {
            Save(model);
        }
        public virtual void Delete(USERMESSAGES entity)
        {
            model.USERMESSAGES.Remove(entity);
            Save(model);
        }
        public virtual IQueryable<USERMESSAGES> GetAll()
        {
            return model.USERMESSAGES.AsQueryable();
        }
        public void Save(AppModell model)
        {
            model.SaveChanges();
        }

        //Finding entity
        public USERMESSAGES FindByID(int ID)
        {
            return this.GetAll().SingleOrDefault(x => x.MessageID == ID);
        }
        public ICollection<USERMESSAGES> FindByUserSent(IUser user)
        {
            return this.GetAll().Where(x => x.SenderID == user.UserID).ToList();
        }
        public ICollection<USERMESSAGES> FindByMatch(IMatch match)
        {
            return this.GetAll().Where(x => x.MatchID == match.MatchID).ToList();
        }
    }
}
