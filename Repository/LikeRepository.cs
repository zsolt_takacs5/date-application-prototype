﻿namespace Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using ApplicationPrototype;
    using AppMutualInterfaces;

    public class LikeRepository : ILikeRepository
    {
        private AppModell model;

        public LikeRepository(AppModell model)
        {
            this.model = model;
        }
        //CRUD methods
        public virtual void Create(LIKES entity)
        {
            model.LIKES.Add(entity);
            Save(model);
        }

        public virtual IQueryable<LIKES> GetAll()
        {
            return model.LIKES.AsQueryable();
        }

        public virtual void Update(LIKES entity)
        {
            Save(model);
        }

        public virtual void Delete(LIKES entity)
        {
            model.LIKES.Remove(entity);
            Save(model);
        }

        public virtual LIKES FindByUsers(IUser judge, IUser judged)
        {
            var like = model.LIKES.SingleOrDefault(x => x.JudgeUser.UserID == judge.UserID && x.JudgedUser.UserID == judged.UserID);
            return like;
        }
        public LIKES FindByID(int likeID)
        {
            return null;
        }
        public void Save(AppModell model)
        {
            model.SaveChanges();
        }
        public virtual ICollection<LIKES> FindAllLikesUserGive(IUser user)
        {
            return GetAll().Where(x => x.Judge == user.UserID).ToList();
        }
        public virtual ICollection<LIKES> FindAllLikesUserGot(IUser user)
        {
            return GetAll().Where(x => x.JudgedPerson == user.UserID).ToList();
        }
        public ICollection<LIKES> FindAllLikesAssignedToUser(IUser user)
        {
            return FindAllLikesUserGot(user).Union(FindAllLikesUserGive(user)).ToList();
        }

    }
}
