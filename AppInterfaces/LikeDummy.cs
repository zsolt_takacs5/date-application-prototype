﻿namespace AppInterfaces
{
    using ApplicationPrototype;

    public class LikeDummy : LIKES
    {
        public int Judge { get; set; }
        public int JudgedPerson { get; set; }
        public bool Liked { get; set; }
    }
}
