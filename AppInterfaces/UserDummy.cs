﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPrototype;
using AppMutualInterfaces;

namespace AppInterfaces
{
    public class UserDummy : USERS, IUser
    {
        public UserDummy()
        {
            
        }
        public int UserID { get => base.UserID; set => base.UserID = value; }
        public string Nickname { get => base.Nickname; set => base.Nickname = value; }
        public int Age { get => base.Age; set => base.Age = value; }
        public string EMail { get => base.EMail; set => base.EMail = value; }
        public bool Male { get => base.Male; set => base.Male = value; }
        public bool Interested { get => base.Interested; set => base.Interested = value; }
        public int MinAge { get => base.MinAge; set => base.MinAge = value; }
        public int MaxAge { get => base.MaxAge; set => base.MaxAge = value; }
        public string Photo1 { get => base.Photo1; set => base.Photo1 = value; }
        public string Photo2 { get => base.Photo2; set => base.Photo2 = value; }
        public string Photo3 { get => base.Photo3; set => base.Photo3 = value; }
        public string Photo4 { get => base.Photo4; set => base.Photo4 = value; }
        public string Photo5 { get => base.Photo5; set => base.Photo5 = value; }
        public string Live1 { get => base.Live1; set => base.Live1 = value; }
        public string Live2 { get => base.Live2; set => base.Live2 = value; }
        public string Live3 { get => base.Live3; set => base.Live3 = value; }
        public bool Verified { get => base.Verified; set => base.Verified = value; }
        public bool WantToDate { get => base.WantToDate; set => base.WantToDate = value; }
        public int? SubscriptionLevel { get => base.SubscriptionLevel; set => base.SubscriptionLevel = value; }
        public DateTime? SubscriptionDate { get => base.SubscriptionDate; set => base.SubscriptionDate = value; }
        public DateTime RegisteredDate { get => base.RegisteredDate; set => base.RegisteredDate = value; }
    }
}
