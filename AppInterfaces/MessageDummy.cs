﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPrototype;

namespace AppInterfaces
{
    public abstract class MessageDummy : USERMESSAGES
    {
        public int MessageID { get; set; }
        public int? MatchID { get; set; }
        public int SenderID { get; set; }
        public DateTime MessageDate { get; set; }
        public string Content { get; set; }
        public string ContentPhoto { get; set; }
        public string ContentType { get; set; }
    }
}
