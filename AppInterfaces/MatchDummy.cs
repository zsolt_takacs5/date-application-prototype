﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationPrototype;

namespace AppInterfaces
{
    public abstract class MatchDummy: MATCHES
    {
        public int MatchID { get; set; }
        public int User1_ID { get; set; }
        public int User2_ID { get; set; }
        public DateTime MatchDate { get; set; }
        public UserDummy UserOne { get; set; }
    }
}
