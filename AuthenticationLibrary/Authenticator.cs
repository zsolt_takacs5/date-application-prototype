﻿
namespace AuthenticationLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IdentityModel.Tokens.Jwt;
    using Microsoft.IdentityModel.Tokens;
    using AppMutualInterfaces;
    using System.Security.Claims;
    using Microsoft.IdentityModel.Logging;

    public class Authenticator
    {
        private const string secretKey = "asdKeymiafaszvanveledtesokam";
        private const string issuer = "DateApplication";

        private byte[] SecurityKeyInBytes
        {
            get { return Encoding.ASCII.GetBytes(secretKey); }
        }

        private JwtSecurityTokenHandler tokenHandler;

        public Authenticator()
        {
            tokenHandler = new JwtSecurityTokenHandler();
        }
        public string CreateJWT(IUser user)
        {

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor();
            SymmetricSecurityKey secKey = new SymmetricSecurityKey(SecurityKeyInBytes);

            tokenDescriptor.Issuer = issuer;
            tokenDescriptor.Expires = DateTime.UtcNow.AddMinutes(30);

            List<Claim> claims = new List<Claim>()
            {
                new Claim("UserID", user.UserID.ToString()),
                new Claim("Email", user.EMail)
            };
            tokenDescriptor.Subject = new ClaimsIdentity(claims);
            tokenDescriptor.SigningCredentials = new SigningCredentials(secKey, SecurityAlgorithms.HmacSha256);

            string tokenke = tokenHandler.CreateEncodedJwt(tokenDescriptor);
            return tokenke; //this creates token: example eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6InRlc3RVc2VyQGdtYWlsLmNvbSIsIk5pY2tOYW1lIjoidGVzdE1hblRoYV9TdXBhIiwibmJmIjoxNTc0Njk0MTk2LCJleHAiOjE1NzQ2OTQ0OTUsImlhdCI6MTU3NDY5NDE5NiwiaXNzIjoiRGF0ZUFwcGxpY2F0aW9uIn0.qwTL7_w82V3jm_aTucbYQnqnHt2423TQrWUqHRkT3UQ
        }
        //TODO 1 IMPORTANT ! - Valdiation must be correct for production: validate audience & lifetime must be true!
        public bool ValidateToken(string tokenString, out ClaimsPrincipal userClaims)
        {
            SecurityToken validatedToken;
            IdentityModelEventSource.ShowPII = true;

            TokenValidationParameters validParameters = new TokenValidationParameters
            {
                ValidIssuer = issuer,
                ValidateAudience = false,
                ValidateLifetime = false,
                IssuerSigningKey = new SymmetricSecurityKey(SecurityKeyInBytes)
            };

            try
            {
                userClaims = tokenHandler.ValidateToken(tokenString, validParameters, out validatedToken);
            }
            catch (Exception e)
            {
                userClaims = null;
            }
            if (userClaims == null)
            {
                return false;
            }
            return true;
        }
        private int GetUserIDFromValidToken(ClaimsPrincipal tokenClaims)
        {
            var userClaims = tokenClaims.Claims.Where(x => x.Type == "UserID");
            return int.Parse(userClaims.FirstOrDefault().Value);
        }
        public ClaimsPrincipal ValidateTokenAndGetClaims(string token)
        {
            ClaimsPrincipal claims = null;
            if (this.ValidateToken(token, out claims))
            {
                return claims;
            }
            else
            {
                return null;
            }
        }
    }

}
