﻿
namespace AuthenticationLibrary
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading;

    //SOURCE: https://www.c-sharpcorner.com/UploadFile/raj1979/authorization-using-web-api/ 
    public class ApplicationAuthenticationHandler : DelegatingHandler
    {
        // Http Response Messages
        private const string InvalidToken = "Invalid Authorization-Token";
        private const string MissingToken = "Missing Authorization-Token";
        protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            System.Threading.CancellationToken cancellationToken)
        {
            IEnumerable<string> sampleApiKeyHeaderValues = null;
            // Checking the Header values
            string token = request.Headers.Authorization.Scheme;
            ClaimsPrincipal claims;
            
            Authenticator authenticator = new Authenticator();
            if (authenticator.ValidateToken(token, out claims))
            {
                System.Web.HttpContext.Current.User = claims;
            }
            else
            {
                // Web request cancel reason APP key missing all parameters
                return RequestCancel(request, cancellationToken, InvalidToken);
            }
            return base.SendAsync(request, cancellationToken);
        }
        private System.Threading.Tasks.Task<HttpResponseMessage> RequestCancel(HttpRequestMessage
request, System.Threading.CancellationToken cancellationToken, string message)
        {
            CancellationTokenSource _tokenSource = new CancellationTokenSource();
            cancellationToken = _tokenSource.Token;
            _tokenSource.Cancel();
            HttpResponseMessage response = new HttpResponseMessage();
            response = new HttpResponseMessage(HttpStatusCode.BadRequest);//request.CreateResponse(HttpStatusCode.BadRequest);
            response.Content = new StringContent(message);
            return base.SendAsync(request, cancellationToken).ContinueWith(task =>
            {
                return response;
            });

        }

    }
}
