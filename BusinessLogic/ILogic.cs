﻿namespace BusinessLogic
{
    using System.Collections.Generic;
    using AppMutualInterfaces;
    using Repository;

    /// <summary>
    /// Represents a set of publicly available functions for manipulating the database.
    /// </summary>
    public interface ILogic
    {

        
        /// <summary>
        /// Logs a user in.
        /// </summary>
        /// <param name="userCredentials">The credentials that the user got.</param>
        /// <returns>The user from the database.</returns>
        IUser LoginUser(ICredentialable userCredentials);

        void ChangeUserPassword(IUser user, string providedPassword, string newPassword);

        string DeletePhoto(IUser user, int location);

        void ChangeUserEmail(IUser user, string providedPassword, string newEmail);
        
        /// <summary>
        /// Register the user by it's parameters
        /// </summary>
        /// <param name="user">the user to be registered.</param>
        /// <param name="photoPath">The path of the first photo.</param>
        void RegisterUser(IRegisterable user, string photoPath);

        /// <summary>
        /// Shares an album between 2 users.
        /// </summary>
        /// <param name="sharerUser">The user who shares his/her album.</param>
        /// <param name="receiverUser">The beneficiary of the album.</param>
        void AlbumShareSecured(IUser sharerUser, IUser receiverUser);

        //Like - Match
        /// <summary>
        /// Add like between 2 users.
        /// </summary>
        /// <param name="sender">The user who sends the like.</param>
        /// <param name="receiver">The user who recieves the like.</param>
        /// <param name="type">The type of like. True if like, False if dislike.</param>
        void AddLikeSecured(IUser sender, IUser receiver, bool type);

        //Messages
        /// <summary>
        /// Sends a message from one user to another. 
        /// </summary>
        /// <param name="sender">The sender of the message.</param>
        /// <param name="receiver">The receiver of the message.</param>
        /// <param name="content">The content of the message.</param>
        /// <param name="pathToPhoto">The path to a photo content of the message. use String.Empty if there isn't any.</param>
        /// <param name="type">The type of the message content.</param>
        IMessage SendMessageSecure(IUser sender, IUser receiver, string content, string pathToPhoto, ContentType type);


        //Retrieve information
        IProfile GetProfileData(IUser submitter, IUser subject, IProfileFactory profileFactory);

        IUser SearchForDate(IUser user);
        IEnumerable<IMessage> GetUnreadMessages(IUser requestSender, IUser receiver);
        void TurnMessageRead(IMessage message, IUser requestSender, IUser receiver);
        void TurnMessageRead(ICollection<IMessage> messageCollection, IUser requestSender, IUser receiver);
        ICollection<IMessage> GetMessages(IUser requestSender, IUser receiver);
        ICollection<IMessage> LoadMatchMessages(IUser userA, IUser userB);
        ICollection<IMessage> LoadMatchMessages(IMatch match);

        Dictionary<IUser, IMessage> LoadAllMatches(IUser user);
        Dictionary<IUser, IMessage> LoadDatePartnerData(IUser user);

        IUser FindUserByID(int ID);
        IUser FindUserByEmail(string emailAdress);

        IUser FindUserByLoginCredentials(ICredentialable user);
        void UpdateProfileData(IUser user);

        void UploadPhoto(IUser user, int location, string photoPath);

        //Future informations to retrieve for subscribed users:
        //ShowWhoLiked(IUser user);
        //BlockUser(IUser blocker, IUser userToBlock);
        //ReportUser(IUser user, string reason);
    }
}
