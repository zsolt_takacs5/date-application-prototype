﻿namespace BusinessLogic
{
    using ApplicationPrototype;
    using AppMutualInterfaces;
    using BusinessLogic.Exceptions;
    using Repository;
    using Repository.Exceptions;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    public class Logic : ILogic, ILogicOperations
    {
        #region Fields
        private IUserRepository userRepo;
        private IMatchRepository matchRepo;
        private ILikeRepository likeRepo;
        private IMessageRepository messageRepo;
        private IDateRepository dateRepo;

        private AppModell model;



        private int messageLimit;
        #endregion

        #region Properties
        public IUserRepository UserRepo { get { return userRepo; } }
        public IMatchRepository MatchRepo { get { return matchRepo; } }
        public ILikeRepository LikeRepo { get { return likeRepo; } }
        public IMessageRepository MessageRepo { get { return messageRepo; } }
        public IDateRepository DateRepo { get { return dateRepo; } }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes new Logic with the given Repositories.
        /// </summary>
        /// <param name="userR">The provided UserRepository.</param>
        /// <param name="matchR">The provided MatchRepository.</param>
        /// <param name="likeR">The provided LikeRepository.</param>
        /// <param name="messageR">The provided MessageRepository.</param>
        /// <param name="dateR">The provided DateRepository.</param>
        public Logic(IUserRepository userR,
            IMatchRepository matchR,
            ILikeRepository likeR,
            IMessageRepository messageR,
            IDateRepository dateR, int messageLimit)
        {
            this.userRepo = userR;
            this.matchRepo = matchR;
            this.likeRepo = likeR;
            this.messageRepo = messageR;
            this.dateRepo = dateR;
            this.messageLimit = messageLimit;
            this.model = new AppModell();
        }
        /// <summary>
        /// Initializes a new Logic with new Repositories provided by the Logic class.
        /// </summary>
        public Logic(int messageLimit)
        {
            
            this.model = new AppModell();
            this.userRepo = new UserRepository(model);
            this.matchRepo = new MatchRepository(model);
            this.likeRepo = new LikeRepository(model);
            this.messageRepo = new MessageRepository(model);
            this.dateRepo = new DateRepository(model);
            this.messageLimit = messageLimit;
            
        }

        #endregion

        public bool IsUserRelated(IUser userA, IUser userB)
        {
            var dbUserA = UserRepo.FindByID(userA.UserID);
            var dbUserB = UserRepo.FindByID(userB.UserID);
            //Are they dating?
            if (dbUserA != null && dbUserB != null && dbUserA.Date != null && dbUserA.Date.USERS.Contains(dbUserB))
            {
                return true;
            }
            //Is there a match between them?
            else if (dbUserA != null && dbUserB != null && MatchRepo.FindByUsers(userA, userB) != null)
            {
                return true;
            }
            //are they the same?
            else if (dbUserA != null && dbUserB != null && dbUserA.UserID == dbUserB.UserID)
            {
                return true;
            }
            return false;
        }
        public IUser LoginUser(ICredentialable userCredentials)
        {
            IUser user;
            try
            {
                user = UserRepo.FindUserByLoginCredentials(userCredentials);
            }
            catch (WrongCredentialsException ex)
            {

                throw new WrongCredentialsException(ex.Message);
            }
            return user;
        }

        public void UploadPhoto(IUser user, int location, string photoPath)
        {
            switch (location)
            {
                case 1:
                    user.Photo1 = photoPath;
                    UpdateProfileData(user);
                    break;
                case 2:
                    user.Photo2 = photoPath;
                    UpdateProfileData(user);

                    break;
                case 3:
                    user.Photo3 = photoPath;
                    UpdateProfileData(user);

                    break;
                case 4:
                    user.Photo4 = photoPath;
                    UpdateProfileData(user);

                    break;
                case 5:
                    user.Photo5 = photoPath;
                    UpdateProfileData(user);

                    break;
                case 6:
                    user.Live1 = photoPath;
                    UpdateProfileData(user);

                    break;
                case 7:
                    user.Live2 = photoPath;
                    UpdateProfileData(user);

                    break;
                case 8:
                    user.Live3 = photoPath;
                    UpdateProfileData(user);

                    break;
                default:
                    break;
            }
        }
        public void TurnMessageRead(IMessage message, IUser requestSender, IUser receiver)
        {
            var messageDB = MessageRepo.FindByID(message.MessageID);
            USERMESSAGES correspondingMessage = null;
            if(messageDB.Match != null)
            {
                //first we need to find the other corresponding match
                var userSender = UserRepo.FindByID(requestSender.UserID);
                var userReceiver = UserRepo.FindByID(receiver.UserID);
                var correspondingMatch = MatchRepo.FindByUsers(userReceiver, requestSender);

                //second we need to find the other corresping message in that match
                correspondingMessage = correspondingMatch.USERMESSAGES.Where(x => x.MessageDate == message.MessageDate).FirstOrDefault();


            }
            messageDB.Read = true;
            //third we need to update it aswell
            if(correspondingMessage != null)
            {
                correspondingMessage.Read = true;
            }
            MessageRepo.Update(messageDB);
            MessageRepo.Update(correspondingMessage);
        }
        public void TurnMessageRead(ICollection<IMessage> messages, IUser requestSender, IUser receiver)
        {
            foreach (var message in messages)
            {
                TurnMessageRead(message, requestSender, receiver);
            }
        }
        public IEnumerable<IMessage> GetUnreadMessages(IUser requestSender, IUser receiver)
        {
            IEnumerable<IMessage> messages = GetMessages(requestSender, receiver)
                .Where(x => x.Read == false)
                .Where(x => x.SenderID == receiver.UserID);
            
            return messages;
        }
        public ICollection<IMessage> GetMessages(IUser requestSender, IUser receiver)
        {
            if (IsUserRelated(requestSender, receiver))
            {
                if (AreUsersDating(requestSender, receiver))
                {
                    var userSender = UserRepo.FindByID(requestSender.UserID);
                    var messages = userSender.Date.USERMESSAGES;
                    List<IMessage> returnMessages = new List<IMessage>();
                    foreach (var message in messages)
                    {
                        returnMessages.Add(message);
                    }
                    return returnMessages;
                }
                //then they must be matched or they are the same user.
                //are they the same? -NO means they matched
                else if(requestSender.UserID != receiver.UserID)
                {
                    return LoadMatchMessages(requestSender, receiver);
                }
                //the sender and receiver are the same
                else
                {
                    throw new NotSupportedActivityException("This activity is not supported due to the sender and the receiver are the same.");
                }
            }
            else
                throw new NotSupportedActivityException("This activity is not supported due to lack of relation between users.");
        }
        public void UpdateProfileData(IUser user)
        {
            var userFromDB = UserRepo.FindByID(user.UserID);
            if (userFromDB == null)
            {
                throw new NotFoundException<IUser>("ID", null);
            }
            userFromDB.Age = user.Age;
            userFromDB.Interested = user.Interested;
            userFromDB.Male = user.Male;
            userFromDB.MaxAge = user.MaxAge;
            userFromDB.MinAge = user.MinAge;
            userFromDB.Nickname = user.Nickname;;
            userFromDB.WantToDate = user.WantToDate;
            UserRepo.Update(userFromDB);
        }
        public IProfile GetProfileData(IUser submitter, IUser subject, IProfileFactory profileFactory)
        {
            //Are users related to each other at all?
            if (IsUserRelated(submitter, subject))
            {
                //Are they dating?
                if (AreUsersDating(submitter, subject))
                {
                    var dbSubmitter = UserRepo.FindByID(submitter.UserID);
                    var dbSubject = UserRepo.FindByID(subject.UserID);

                    IProfile profile = null;
                    if (dbSubject.AlbumSharedWith.Contains(dbSubmitter) || dbSubmitter.AlbumSharedBy.Contains(dbSubject))
                    {
                        profile = profileFactory.GetProfile(subject, ProfileType.FullProfile);
                    }
                    else
                    {
                        profile = profileFactory.GetProfile(subject, ProfileType.RestrictedProfile); 
                    }
                    profile.Abilities = GetAbilities(submitter, subject);
                    return profile;
                }
                //they have matched
                else if (MatchRepo.FindByUsers(submitter, subject) != null)
                {
                    var profile = profileFactory.GetProfile(subject, ProfileType.FullProfile);
                    profile.Abilities = GetAbilities(submitter, subject);
                    return profile;
                }
                //they dont have a match because they are the same - The request was trying to get access to his own profile data.
                else
                {
                    return profileFactory.GetProfile(subject, ProfileType.FullUserProfile);
                }
            }
            else
            {
                throw new NotSupportedActivityException("Can not view this profile's data due to lack of relation.");
            }
        }
        public Dictionary<string, bool> GetAbilities(IUser requestor, IUser requested)
        {
            Dictionary<string, bool> abilities = new Dictionary<string, bool>();
            abilities.Add("LikeAble", false);
            abilities.Add("ShareAlbum", false);
            abilities.Add("DislikeAble", true);

            if (AreUsersDating(requestor, requested))
            {
                if (IsMessageLimitReached(requestor, requested))
                {
                    var requestorDB = UserRepo.FindByID(requestor.UserID);
                    var requestedDB = UserRepo.FindByID(requested.UserID);

                    //are the requestor liked the other? - no: add like ability
                    if (!(requestorDB.Likes.Where(x=>x.JudgedPerson == requestedDB.UserID).Count() > 0))
                    {
                        //abilities.Add("LikeAble", true);
                        abilities["LikeAble"] = true;
                    }

                    //are the requestor shared the album? - no: add share ability
                    if (!(requestorDB.AlbumSharedWith.Where(x=>x.UserID == requestedDB.UserID).Count() > 0))
                    {
                        //abilities.Add("ShareAlbum", true);
                        abilities["ShareAlbum"] = true;
                    }
                }
            }

            return abilities;
        }
        public void AlbumShare(IUser sharerUser, IUser receiverUser)
        {
            var sharer = UserRepo.FindByID(sharerUser.UserID);
            var receiver = UserRepo.FindByID(receiverUser.UserID);

            sharer.AlbumSharedWith.Add(receiver);
            UserRepo.Update(sharer);
        }
        public void AlbumShareSecured(IUser sharerUser, IUser receiverUser)
        {
            var sharer = UserRepo.FindByID(sharerUser.UserID);
            var receiver = UserRepo.FindByID(receiverUser.UserID);
            if (sharer != null && receiver != null)
            {
                if (IsUserRelated(sharerUser, receiverUser))
                {
                    if (AreUsersDating(sharerUser, receiverUser))
                    {
                        if (IsMessageLimitReached(sharerUser, receiverUser))
                        {
                            if (!sharer.AlbumSharedWith.Contains(receiver))
                            {
                                AlbumShare(sharer, receiver);
                                return;
                            }
                            throw new NotSupportedActivityException("Can not share album, because album has already been shared.");
                        }
                        throw new NotSupportedActivityException("Can not share album, due to messages need to reach the limit first.");
                    }
                    throw new NotSupportedActivityException("Can not share album, because the users are not dating right now.");
                }
                throw new NotSupportedActivityException("Can not share album. The users are not related at all.");
            }
            throw new NotFoundException<IUser>("UserID", null);
        }

        //User
        /// <summary>
        /// This method saves the user to the database with the given user credentials. User has to have an unused e-mail address.
        /// </summary>
        /// <param name="userCredentials">The user's data get from client application as JSON in the body of HTTP.</param>
        /// <exception cref="MultipleEmailException">Thrown when there is already an account with the provided e-mail address in the database.</exception>
        public void RegisterUser(IRegisterable userCredentials, string photoPath)
        {
            var dbUser = UserRepo.FindUserByEmail(userCredentials.EMail);

            if (dbUser == null)
            {
                if (!string.IsNullOrEmpty(photoPath))
                {
                    string passsword = UserRepo.HashPassword(userCredentials.PassW);
                    var newUser = new USERS()
                    {
                        Age = userCredentials.Age,
                        EMail = userCredentials.EMail,
                        Interested = userCredentials.Interested,
                        LikedBy = new List<LIKES>(),
                        Likes = new List<LIKES>(),
                        Live1 = String.Empty,
                        Live2 = String.Empty,
                        Live3 = String.Empty,
                        Male = userCredentials.Gender,
                        MATCHES = new List<MATCHES>(),
                        MaxAge = userCredentials.MaxAge,
                        MinAge = userCredentials.MinAge,
                        Nickname = userCredentials.Name,
                        PassW = UserRepo.HashPassword(userCredentials.PassW),
                        Photo1 = photoPath,
                        Photo2 = String.Empty,
                        Photo3 = String.Empty,
                        Photo4 = String.Empty,
                        Photo5 = String.Empty,
                        RegisteredDate = DateTime.Now,
                        SubscriptionDate = DateTime.Now,
                        SubscriptionLevel = 0,
                        USERMESSAGES = new List<USERMESSAGES>(),
                        Verified = false,
                        WantToDate = false
                    };
                    UserRepo.Create(newUser); 
                }
                else
                {
                    throw new ArgumentNullException("A photo must be provided before registration!");
                }
            }
            else
            {
                throw new MultipleEmailException("There is already an account with the provided e-mail address.");
            }
        }

        //Like - Match
        //TODO DONE - Handling like if sender has already liked the user => throw exception.
        /// <summary>
        /// Adds a like between two users, only if they are dating. Otherwise throw NotSupportedActivity.
        /// </summary>
        /// <param name="sender">The sender of the like.</param>
        /// <param name="receiver">The receiver of the like.</param>
        /// <param name="type">The type of like. True: liked, False: disliked.</param>
        /// <exception cref="NotSupportedActivity">Thrown when the users are not dating.</exception>
        public void AddLikeSecured(IUser sender, IUser receiver, bool type)
        {
            var userSender = UserRepo.FindByID(sender.UserID);
            if (IsUserRelated(sender, receiver))
            {

                if (this.AreUsersDating(sender, receiver))
                {
                    if (!IsMessageLimitReached(sender, receiver) && type == true)
                    {
                        throw new NotSupportedActivityException("This activity is not supported. Reason: Can not like another user, when messages don't reached the limit");
                    }

                    if (userSender.Likes.Where(x=>x.JudgedPerson == receiver.UserID).Count() > 0)
                    {
                        throw new NotSupportedActivityException("This activity is not supported. Reason: Can not give more than 1 like to another user.");
                    }
                    else
                    {
                        AddLike(sender, receiver, type);
                    }
                }
                else
                {
                    throw new NotSupportedActivityException("This activity is not supported. Reason: The users are not dating.");
                }
            }
            else
            {
                throw new NotSupportedActivityException("This activity is not supported. Reason: The users are not related at all.");
            }
        }
        public string DeletePhoto(IUser user, int location)
        {
            var userDB = UserRepo.FindByID(user.UserID);
            string filePath = string.Empty;
            if (userDB != null)
            {
                switch (location)
                {
                    case 1:
                        filePath = userDB.Photo1;
                        userDB.Photo1 = string.Empty;
                        UserRepo.Update(userDB);
                        break;
                    case 2:
                        filePath = userDB.Photo2;
                        userDB.Photo2 = string.Empty;
                        UserRepo.Update(userDB);
                        break;
                    case 3:
                        filePath = userDB.Photo3;
                        userDB.Photo3 = string.Empty;
                        UserRepo.Update(userDB);
                        break;
                    case 4:
                        filePath = userDB.Photo4;
                        userDB.Photo4 = string.Empty;
                        UserRepo.Update(userDB);
                        break;
                    case 5:
                        filePath = userDB.Photo5;
                        userDB.Photo5 = string.Empty;
                        UserRepo.Update(userDB);
                        break;
                }
            }
            return filePath;
        }
        public void AddLike(IUser sender, IUser receiver, bool type)
        {

            //NEW
            LIKES l1 = new LIKES()
            {
                Judge = sender.UserID,
                JudgedPerson = receiver.UserID,
                JudgeUser = UserRepo.FindByID(sender.UserID),
                JudgedUser = UserRepo.FindByID(receiver.UserID),
                Liked = type
            };

            LikeRepo.Create(l1);
            
            //TODO check this if statement for errors - take a look at FRONT-END: when dislike, must call Date endpoint to get a new date partner and turn wanttodate to true;
            if (type == false)
            {
                var senderDB = UserRepo.FindByID(sender.UserID);
                var receiverDB = UserRepo.FindByID(receiver.UserID);

                DateRepo.Delete(senderDB.Date);

                senderDB.WantToDate = false;
                receiverDB.WantToDate = false;

                UserRepo.Update(senderDB);
                UserRepo.Update(receiverDB);
                return;
            }

            var like = LikeRepo.FindByUsers(receiver, sender);
            if (like != null && like.Liked == true && type == true)
            {
                AddMatch(sender, receiver);

                //Remove date from users & database
                var user1 = UserRepo.FindByID(sender.UserID);
                var user2 = UserRepo.FindByID(receiver.UserID);
                if (user1 != null && user2 != null && user1.Date != null && user2.Date != null)
                {
                    user1.WantToDate = false;
                    user2.WantToDate = false;

                    UserRepo.Update(user1);
                    UserRepo.Update(user2);

                    DateRepo.Delete(user1.Date);
                }
            }
        }
        public void AddMatch(IUser sender, IUser receiver)
        {
            //NEW
            DateTime now = DateTime.Now;
            var senderUser = UserRepo.FindByID(sender.UserID);
            var receiverUser = UserRepo.FindByID(receiver.UserID);

            //Sharing albums
            try
            {
                AlbumShare(senderUser, receiverUser);
                AlbumShare(receiverUser, senderUser);
            }
            catch (NotSupportedActivityException ex)
            {

                throw;
            }

            var message1 = senderUser.USERMESSAGES.Where(x => x.Match == null).AsQueryable();
            var message2 = receiverUser.USERMESSAGES.Where(x => x.Match == null).AsQueryable();
            var union = message1.Union(message2);

            MATCHES m1 = new MATCHES()
            {
                MatchDate = now,
                User1_ID = sender.UserID,
                User2_ID = receiver.UserID,
                USERMESSAGES = message1.ToList(),
                USER = UserRepo.FindByID(sender.UserID)
            };
            MATCHES m2 = new MATCHES()
            {
                MatchDate = now,
                User1_ID = receiver.UserID,
                User2_ID = sender.UserID,
                USERMESSAGES = message2.ToList(),
                USER = UserRepo.FindByID(receiver.UserID)
            };
            MatchRepo.Create(m1);
            MatchRepo.Create(m2);
            senderUser.Date = null;
            receiverUser.Date = null;
            UserRepo.Update(senderUser);
            UserRepo.Update(receiverUser);
        }

        //Message
        public bool IsMessageLimitReached(IUser userA, IUser userB)
        {
            if (IsUserRelated(userA, userB))
            {
                if (this.AreUsersDating(userA, userB))
                {
                    int count = UserRepo.FindByID(userA.UserID).Date.USERMESSAGES.Count;
                    return count >= this.messageLimit;
                }
                return true;
            }
            return false;
        }
        public IMessage SendMessageSecure(IUser sender, IUser receiver, string content, string pathToPhoto, ContentType type)
        {
            if (this.AreUsersDating(sender, receiver))
            {
                return SendMessage(sender, receiver, content, pathToPhoto, type);
            }
            else if (MatchRepo.FindByUsers(sender, receiver) != null)
            {
                var match = MatchRepo.FindByUsers(sender, receiver);
                return SendMessage(sender, match, content, pathToPhoto, type);
            }
            else
            {
                throw new NotSupportedActivityException("This activity is not supported. Reason: The receiver is not related to the sender at all.");
            }
        }
        public bool AreUsersDating(IUser userA, IUser userB)
        {
            var dbUserA = UserRepo.FindByID(userA.UserID);
            var dbUserB = UserRepo.FindByID(userB.UserID);
            return dbUserA != null && dbUserB != null && dbUserA.Date !=null && dbUserA != dbUserB && dbUserA.Date.USERS.Contains(dbUserB);
        }
        public IMessage SendMessage(IUser sender, IUser receiver, string content, string pathToPhoto, ContentType type)
        {
            bool areDating = this.AreUsersDating(sender, receiver);
            USERMESSAGES message;
            if (areDating)
            {
                message = new USERMESSAGES()
                {
                    Content = content,
                    ContentPhoto = pathToPhoto,
                    ContentType = type.ToString(),
                    Match = null,
                    MessageDate = DateTime.Now,
                    SenderID = sender.UserID,
                    Date = UserRepo.FindByID(sender.UserID).Date,
                    Read = false
                };
            }
            else
            {
                var match = matchRepo.FindByUsers(sender, receiver);
                message = new USERMESSAGES()
                {
                    Content = content,
                    ContentPhoto = pathToPhoto,
                    ContentType = type.ToString(),
                    Match = match,
                    MatchID = match.MatchID,
                    MessageDate = DateTime.Now,
                    SenderID = sender.UserID,
                    Date = null,
                    Read = false
                };
            }
            MessageRepo.Create(message);

            //Should return the message
            /*
                1. Identify the newly inserted message
                    - if the message has a date: find last message of the date
                    - if the message has a match: find last message of the match
                2. Return the message to the TOP
             */
            if (message.Date != null)
            {
                return UserRepo.FindByID(sender.UserID).Date.USERMESSAGES.First();
            }
            else //they are matched
                return MatchRepo.FindByID(message.Match.MatchID).USERMESSAGES.First();
            
        }
        
        public IMessage SendMessage(IUser sender, IMatch match, string content, string pathToPhoto, ContentType type)
        {
            //OLD
            var senderUser = UserRepo.FindByID(sender.UserID);
            var receiverUser = UserRepo.FindByID(match.User2_ID);

            var matchSender = MatchRepo.FindByUsers(senderUser, receiverUser);
            var matchReceiver = MatchRepo.FindByUsers(receiverUser, senderUser);

            var message1 = new USERMESSAGES()
            {
                Content = content,
                ContentPhoto = pathToPhoto,
                ContentType = type.ToString(),
                MatchID = matchSender.MatchID,
                MessageDate = System.DateTime.Now,
                SenderID = senderUser.UserID
            };

            var message2 = new USERMESSAGES()
            {
                Content = content,
                ContentPhoto = pathToPhoto,
                ContentType = type.ToString(),
                MatchID = matchReceiver.MatchID,
                MessageDate = System.DateTime.Now,
                SenderID = sender.UserID
            };

            MessageRepo.Create(message1);
            MessageRepo.Create(message2);
            //UserRepo.Update(user);

            var returnMatch = MatchRepo.FindByUsers(senderUser, receiverUser);
            var returnMatchMessages = returnMatch.USERMESSAGES;


            var returnedMessage = returnMatchMessages.First();

            return returnedMessage;
        }

        //Retrieve information
        //DEPRECTED
        public IUser GetDateDeprecated(IUser userEntity)
        {
            /*
             * 
             *      Retrieve all the users from DB
             *      Select user based on both's preferences
             *      If succes: return the selected user
             *      If not: return null.
             */


            //new
            var user = UserRepo.FindByID(userEntity.UserID);
            USERS date = null;
            if (user.Date == null)
            {
                var users = UserRepo.GetAll();
                var possibleDates = users.Where(x => x.WantToDate == true)
                    .Where(x => x.Male == user.Interested && x.Interested == user.Male)
                    .Where(x => x.MinAge < user.Age && x.MaxAge > user.Age && user.MinAge < x.Age && user.MaxAge > x.Age); //return the possible users based on both preferences
                int i = 0;

                //Search in these users if they are liked each other or not.
                do
                {
                    if (possibleDates.ElementAt(i).Likes.Where(x => x.JudgedPerson == user.UserID).Count() == 0 &&
                        possibleDates.ElementAt(i).LikedBy.Where(x => x.Judge == user.UserID).Count() == 0)
                    {
                        date = possibleDates.ElementAt(i);
                    }
                    i++;
                } while (i < possibleDates.Count() && date == null);
            }
            else
            {
                date = user.Date.USERS.Where(x => x.UserID != user.UserID).First();
            }
            return date;
        }
        public IUser GetDate(IUser initiator)
        {
            var user = UserRepo.FindByID(initiator.UserID);

            //if has a date partner, return it back
            if (user.Date != null)
            {
                return user.Date.USERS.Where(x => x.UserID != user.UserID).SingleOrDefault();
            }
            else
            {
                user.WantToDate = true;
                UserRepo.Update(user);

                int laps = 0;
                do
                {
                    
                    laps = laps + 1;
                    var possibleDatePartners = GetAllPossibleDatePartners(initiator);

                    while (possibleDatePartners.Count > 0)
                    {
                        IUser randomPartner = null;
                        Random r = new Random();
                        do
                        {
                            randomPartner = possibleDatePartners[r.Next(0, possibleDatePartners.Count)];
                            possibleDatePartners.Remove(randomPartner);
                            
                        } while (possibleDatePartners.Count > 0 && !ArePreferencesMatch(initiator, randomPartner));

                        if (possibleDatePartners.Count == 0)
                        {
                            if (ArePreferencesMatch(initiator, randomPartner))
                            {
                                try
                                {
                                    return RandevouMaker(initiator, randomPartner);
                                }
                                catch (AlreadyHasDatePartnerException ex)
                                {
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                return RandevouMaker(initiator, randomPartner);
                            }
                            catch (AlreadyHasDatePartnerException)
                            {
                                continue;
                            }
                        }
                    }
                    Thread.Sleep(1000);
                } while (laps < 5);

                user.WantToDate = false;
                UserRepo.Update(user);
                throw new DatePartnerNotFoundException("Can not found a partner... Please try changing your preferences.");
            }
        }
        public IUser SearchForDate(IUser initiator)
        {
            try
            {
                return GetDate(initiator);
            }
            catch (DatePartnerNotFoundException ex)
            {

                throw;
            }
        }
        /// <summary>
        /// Searches a date partner for a specific user. 
        /// </summary>
        /// <param name="userEntity">The user who needs a date partner.</param>
        /// <returns>The date partner.</returns>
        /// <exception cref="DatePartnerNotFoundException">Thrown when the date partner can not be found.</exception>
        /// <exception cref="AlreadyHasDatePartnerException">Thrown when the user </exception>
        public IUser SearchForDateDeprecated(IUser userEntity)
        {
            var user = UserRepo.FindByID(userEntity.UserID);
            user.WantToDate = true;
            UserRepo.Update(user);

            var date = GetDate(userEntity);
            if (date == null)
            {
                user.WantToDate = false;
                UserRepo.Update(user);
                throw new DatePartnerNotFoundException("Date partner can not be found. Please try again later, or please recalculate your specifications");
            }
            else if (date.WantToDate == true && userEntity.WantToDate == true)
            {
                var dating = new DATINGS()
                {
                    USERMESSAGES = new List<USERMESSAGES>(),
                    USERS = new List<USERS>()

                };
                dating.USERS.Add(UserRepo.FindByID(userEntity.UserID));
                dating.USERS.Add(UserRepo.FindByID(date.UserID));
                date.WantToDate = false;
                userEntity.WantToDate = false;
                DateRepo.Create(dating);
            }
            else if (userEntity.WantToDate == false)
            {
                user.WantToDate = false;
                UserRepo.Update(user);
                throw new AlreadyHasDatePartnerException("User has been added to a Date during the search.");
            }
            return date;
        }

        public Dictionary<IUser, IMessage> LoadAllMatches(IUser user)
        {
            Dictionary<IUser, IMessage> matchesAndMessages = new Dictionary<IUser, IMessage>();
            var userEntity = UserRepo.FindByID(user.UserID);
            var matches = userEntity.MATCHES;
            foreach (var match in matches)
            {
                if (match.USERMESSAGES.Count > 0)
                {
                    matchesAndMessages.Add(UserRepo.FindByID(match.User2_ID), match.USERMESSAGES.Last()); 
                }
                else
                {
                    matchesAndMessages.Add(UserRepo.FindByID(match.User2_ID), null);
                }
            }
            return matchesAndMessages;
        }

        public Dictionary<IUser, IMessage> LoadDatePartnerData(IUser user)
        {
            Dictionary<IUser, IMessage> dateData = new Dictionary<IUser, IMessage>();
            var userEntity = UserRepo.FindByID(user.UserID);
            if(userEntity.Date != null)
            {
                dateData.Add(userEntity.Date.USERS.Where(x => x.UserID != user.UserID).SingleOrDefault(),userEntity.Date.USERMESSAGES.Count() > 0 ? userEntity.Date.USERMESSAGES.Last() : null);
            }
            return dateData;
        }

        public ICollection<IUser> LoadMatches(IUser user)
        {
            var userEntity = UserRepo.FindByID(user.UserID);
            var matches = userEntity.MATCHES;
            ICollection<IUser> users = new List<IUser>();

            foreach (var match in matches)
            {
                users.Add(UserRepo.FindByID(match.User2_ID));
            }
            return users;
        }
        public ICollection<IMessage> LoadMatchMessages(IUser userA, IUser userB)
        {
            //NEW
            var m1 = MatchRepo.FindByUsers(userA, userB);
            var m2 = MatchRepo.FindByUsers(userB, userA);

            var messages1 = m1.USERMESSAGES;
            var messages2 = m2.USERMESSAGES;

            List<IMessage> messages = new List<IMessage>();
            
            foreach (var message in messages1)
            {
                messages.Add(message);
            }
            //TODO - need to rearrange them in the correct order.
            messages.OrderBy(x => x.MessageDate);

            return messages;
            
        }
        public ICollection<IMessage> LoadMatchMessages(IMatch match)
        {
            //AppModell model = new AppModell();
            //var user1 = model.USERS.Single(x => x.UserID == match.USER.UserID);
            //var user2 = model.USERS.Single(x => x.UserID == match.User2_ID);
            var user1 = UserRepo.GetAll().SingleOrDefault(x => x.UserID == match.User1_ID);
            var user2 = UserRepo.GetAll().SingleOrDefault(x => x.UserID == match.User2_ID);
            if (user1 != null && user2 != null)
                return this.LoadMatchMessages(user1, user2).ToList();
            else
                return null;
        }

        /// <summary>
        /// Finds a User by its given ID.
        /// </summary>
        /// <param name="ID">The ID of the user.</param>
        /// <returns>Returns <see cref="IUser"/> entity. </returns>
        /// <throws>Thrown when User can not been found in the database with the given ID.</throws>
        public IUser FindUserByID(int ID)
        {
            IUser user = UserRepo.FindByID(ID);
            
            if (user == null)
            {
                throw new NotFoundException<IUser>("ID", null);
            }
            return user;
        }
        public IUser FindUserByEmail(string emailAdress)
        {
            return UserRepo.FindUserByEmail(emailAdress);
        }
        public IUser FindUserByLoginCredentials(ICredentialable user)
        {
            IUser userFromDB;
            try
            {
                userFromDB = UserRepo.FindUserByLoginCredentials(user);
            }
            catch (WrongCredentialsException ex)
            {

                throw ex;
            }
            return userFromDB;
        }

        public void ChangeUserEmail(IUser user, string providedPassword, string newEmail)
        {
            var userFromDB = UserRepo.FindByID(user.UserID);
            if (UserRepo.VerifyHashedPassword(userFromDB.PassW, providedPassword))
            {
                if (UserRepo.IsValidEmail(newEmail))
                {
                    if (UserRepo.FindUserByEmail(newEmail) == null)
                    {
                        userFromDB.EMail = newEmail;
                        UserRepo.Update(userFromDB);
                        return;
                    }
                    else
                    {
                        throw new MultipleEmailException("There is already a user associated with this e-mail address");
                    }
                }
                throw new NotSupportedActivityException("E-mail is not valid, or it has already been used by someone else.");

            }
            throw new WrongCredentialsException("Can not change the e-mail address due to password mismatch.");
        }

        public void ChangeUserPassword(IUser user, string providedPassword, string newPassword)
        {
            var userFromDB = UserRepo.FindByID(user.UserID);
            if (UserRepo.VerifyHashedPassword(userFromDB.PassW, providedPassword))
            {
                userFromDB.PassW = UserRepo.HashPassword(newPassword);
                UserRepo.Update(userFromDB);
            }
            else
            {
                throw new WrongCredentialsException("The provided password is invalid!");
            }
        }

        public IUser RandevouMaker(IUser initiator, IUser possiblePartner)
        {
            var partner = UserRepo.FindByID(possiblePartner.UserID);
            var user = UserRepo.FindByID(initiator.UserID);
            if (partner.Date == null)
            {
                if (user.Date == null)
                {
                    user.WantToDate = false;
                    partner.WantToDate = false;
                    UserRepo.Update(user);
                    UserRepo.Update(partner);
                    DateRepo.Create(new DATINGS() { USERS = new List<USERS>() { user, partner } });
                    return partner;
                }
                else
                {
                    return user.Date.USERS.Where(x => x.UserID != user.UserID).SingleOrDefault();
                }
            }
            

            throw new AlreadyHasDatePartnerException("The possible partner already has a date partner.");
        }

        public List<IUser> GetAllPossibleDatePartners(IUser initiator)
        {
            var users = UserRepo.GetAll();
            var possibleDates = users.Where(x => x.WantToDate == true)
                    .Where(x => x.Male == initiator.Interested) //&& x.Interested == initiator.Male)
                    
                    .Where(x=> initiator.MinAge < x.Age &&
                                initiator.MaxAge > x.Age);
                    
            return possibleDates.ToList<IUser>();
        }

        public bool ArePreferencesMatch(IUser initiator, IUser possiblePartner)
        {
            return initiator.Male == possiblePartner.Interested &&
                initiator.Interested == possiblePartner.Male &&
                initiator.Age > possiblePartner.MinAge &&
                initiator.Age < possiblePartner.MaxAge &&
                initiator.MinAge < possiblePartner.Age &&
                initiator.MaxAge > possiblePartner.Age;
        }
    }
}
