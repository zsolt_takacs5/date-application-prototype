﻿namespace BusinessLogic
{

    using AppMutualInterfaces;
    using Repository;
    using System.Collections.Generic;

    /// <summary>
    /// Hidden operations on the database 
    /// </summary>
    internal interface ILogicOperations
    {
        /// <summary>
        /// The user repository for handling database models.
        /// </summary>
        IUserRepository UserRepo { get; }
        /// <summary>
        /// The match repository for handling database models.
        /// </summary>
        IMatchRepository MatchRepo { get; }
        /// <summary>
        /// The message repository for handling database models.
        /// </summary>
        IMessageRepository MessageRepo { get; }
        /// <summary>
        /// The like repository for handling database models.
        /// </summary>
        ILikeRepository LikeRepo { get; }
        /// <summary>
        /// The date repository for handling database models.
        /// </summary>
        IDateRepository DateRepo { get; }

        /// <summary>
        /// Tells if 2 users are related to each other or not.
        /// </summary>
        /// <param name="userA">The first user.</param>
        /// <param name="userB">The second user.</param>
        /// <returns>True if they are related. False if they are not related.</returns>
        bool IsUserRelated(IUser userA, IUser userB);
        /// <summary>
        /// Tells if 2 users reached the message limit.
        /// </summary>
        /// <param name="userA">The first user of the contact.</param>
        /// <param name="userB">The second user of the contact.</param>
        /// <returns>True if the count of messages between 2 users have reached the limit. False if the limit has not reached.</returns>
        bool IsMessageLimitReached(IUser userA, IUser userB);
        bool AreUsersDating(IUser userA, IUser userB);
        IUser GetDate(IUser user); //To Internal
        void AddLike(IUser sender, IUser receiver, bool type);
        void AddMatch(IUser sender, IUser receiver);
        void AlbumShare(IUser sharerUser, IUser receiverUser);
        IMessage SendMessage(IUser sender, IUser receiver, string content, string pathToPhoto, ContentType type);
        IMessage SendMessage(IUser sender, IMatch match, string content, string pathToPhoto, ContentType type);

        IUser RandevouMaker(IUser initiator, IUser possiblePartner);
        List<IUser> GetAllPossibleDatePartners(IUser initiator);
        bool ArePreferencesMatch(IUser initiator, IUser possiblePartner);
        

        ICollection<IUser> LoadMatches(IUser userA);

        Dictionary<string, bool> GetAbilities(IUser requestor, IUser requested);

    }
}
