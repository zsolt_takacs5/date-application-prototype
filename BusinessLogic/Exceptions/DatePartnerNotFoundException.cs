﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Exceptions
{
    public class DatePartnerNotFoundException : Exception
    {
        public DatePartnerNotFoundException(string message) : base(message)
        {

        }
    }
}
