﻿using System;

namespace BusinessLogic.Exceptions
{
    public class NotSupportedActivityException : Exception
    {
        public NotSupportedActivityException(string message) : base(message)
        {

        }
    }
}
